<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
//untuk mengetahui bulan bulan
if ( ! function_exists('bulan'))
{
    function bulan($bln)
    {

        switch ((int)$bln)
        {
            case 1:
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "November";
                break;
            case 12:
                return "Desember";
                break;
        }
    }
}
 
//format tanggal mm yy
if ( ! function_exists('laporan_bulan'))
{
    function laporan_bulan($tgl)
    {
		$tgl = substr($tgl,5,2);
		switch ((int)$tgl)
        {
            case 1:
                $tgl = "Januari";
                break;
            case 2:
                $tgl = "Februari";
                break;
            case 3:
                $tgl = "Maret";
                break;
            case 4:
                $tgl = "April";
                break;
            case 5:
                $tgl = "Mei";
                break;
            case 6:
                $tgl = "Juni";
                break;
            case 7:
                $tgl = "Juli";
                break;
            case 8:
                $tgl = "Agustus";
                break;
            case 9:
                $tgl = "September";
                break;
            case 10:
                $tgl = "Oktober";
                break;
            case 11:
                $tgl = "November";
                break;
            case 12:
                $tgl = "Desember";
                break;
        }
        return $tgl.' '.substr($tgl,0,4);
    }
}
 
//format tanggal yyyy-mm-dd
if ( ! function_exists('tgl_indo'))
{
    function tgl_indo($tgl)
    {
        $ubah = gmdate($tgl, time()+60*60*8);
        $pecah = explode("-",$ubah);  //memecah variabel berdasarkan -
        $tanggal = $pecah[2];
        $bulan = bulan($pecah[1]);
        $tahun = $pecah[0];
        return $tanggal.' '.$bulan.' '.$tahun; //hasil akhir
    }
}
 
//format tanggal timestamp
if( ! function_exists('tgl_indo_timestamp')){
     
    function tgl_indo_timestamp($tgl)
    {
        $inttime=date('Y-m-d H:i:s',strtotime($tgl)); //mengubah format menjadi tanggal biasa
        $tglBaru=explode(" ",$inttime); //memecah berdasarkan spaasi
         
        $tglBaru1=$tglBaru[0]; //mendapatkan variabel format yyyy-mm-dd
        $tglBaru2=$tglBaru[1]; //mendapatkan fotmat hh:ii:ss
        $tglBarua=explode("-",$tglBaru1); //lalu memecah variabel berdasarkan -
     
        $tgl=$tglBarua[2];
        $bln=$tglBarua[1];
        $thn=$tglBarua[0];
     
        $bln=bulan($bln); //mengganti bulan angka menjadi text dari fungsi bulan
        $ubahTanggal="$tgl $bln $thn | $tglBaru2 "; //hasil akhir tanggal
     
        return $ubahTanggal;
    }
}

if( ! function_exists('rupiah')){
    function rupiah($nilai, $pecahan = 0){
       return number_format($nilai, $pecahan, ',', '.');
    }
}