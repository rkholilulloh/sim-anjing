<?php
function get_luas_wilayah(){
    $CI=&get_instance();
    return $CI->db->select('id,nama_wilayah')->where('active',1)->get('ref_potensi_wilayah')->result();
}

function get_logo_desa($kode_wilayah){
    $CI=&get_instance();
    $logo = $CI->db->select('logo')->where('kode_desa',$kode_wilayah)->get('profil_desa')->row()->logo;
    return base_url('assets/uploads/image_logo/'.$kode_wilayah.'/'.$logo);
}
?>