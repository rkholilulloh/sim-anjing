<?php
   function tanggal_format($date){
      $BulanIndo = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "  Desember");

      $tahun = substr($date, 0, 4);
      $bulan = substr($date, 5, 2);
      $tgl   = substr($date, 8, 2);
      $result = $tgl . " " . $BulanIndo[(int)$bulan-1] . " ". $tahun;

      return($result);
   }

   function rubah_tanggal($date){
      $pecah=explode('-',$date);
      return $pecah[2]."-".$pecah[1]."-".$pecah[0];
   }

   function rupiah($value, $rp=''){
      if($value){
         if(!$rp){
         $formated = str_replace(',', '.', number_format($value));
         return $formated;
      }else{
         $formated = 'Rp '.str_replace(',', '.', number_format($value));
         return $formated;
      }
      }else{
         return '-';
      }
   }
?>