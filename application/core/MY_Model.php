<?php

class MY_Model extends CI_Model {

    

    protected $_table_name = '';

    protected $_primary_key = 'id';

    protected $_order_by = '';

    public $rules = array();

    protected $_timestamps = FALSE;

    public $data = array();

    

    function __construct(){

        parent::__construct();

    }

    

	

	//insert_data

	public function insert($data) {

		$this->db->set($data);

		$this->db->insert($this->_table_name);

		$id = $this->db->insert_id();

		return $id;

	}

	

	//update_data

	public function update($id = NULL, $data) {

		if($id != NULL) {

			$id = intval($id);

			$this->db->where($this->_primary_key, $id);

			$this->db->set($data);

			$update = $this->db->update($this->_table_name);

			return $update;

		}

	}

	

	//delete_data

    public function delete($id) {

		$id = intval($id);

        $this->db->where($this->_primary_key, $id);

        $delete = $this->db->delete($this->_table_name);

		return $delete;

	}

		

	//update_active_to_delete

    public function update_delete($id) {

		$id = intval($id);

        $this->db->where($this->_primary_key, $id);

        $this->db->set('active','0');

		$update = $this->db->update($this->_table_name);

		return $update;

	}

	

	//get_all_data

    public function get_all($id = NULL) {

		if ($id != NULL) {

            $id = intval($id);

            $this->db->where($this->_primary_key, $id);

		}

		

		$data = $this->db->get($this->_table_name);

		return $data;

	}

	

	//get_data_active

    public function get($id = NULL) {

		if ($id != NULL) {

            $id = intval($id);

            $this->db->where($this->_primary_key, $id);

		}

		

		$this->db->where('active','1');

		$data = $this->db->get($this->_table_name);

		return $data;

	}

	

	//getdataby

    public function get_by($condition) {

		$this->db->where($condition);

		$data = $this->db->get($this->_table_name);

		return $data;

	}

	

	public function get_modal($id_kecamatan){

		$query = $this->db->query("select * from ".$this->_table_name." 

			left join ref_kecamatan on ref_kecamatan.id_kecamatan=".$this->_table_name.".id_kecamatan

			left join ref_kelurahan on ref_kelurahan.id_kelurahan=".$this->_table_name.".id_kelurahan

			where ".$this->_table_name.".id_kecamatan='".$id_kecamatan."'");

		return $query;

	}


	public function get_laporan(){

		$query = $this->db->query("select nama_kecamatan,".$this->_table_name.".id_kecamatan as id,count(*) as jumlah from ".$this->_table_name." 

				join ref_kecamatan on ref_kecamatan.id_kecamatan=".$this->_table_name.".id_kecamatan

				group by ".$this->_table_name.".id_kecamatan");

		return $query;

	}



	//postfromview

    public function array_from_post($fields) {

        $data = array();

        foreach ($fields as $field) {

            $data[$field] = $this->input->post($field, TRUE);

        }

        return $data;

    }

	

	//encryption

    public function hash($string){

        return hash('sha512', $string . config_item('encryption_key'));

    }

}