<?php

class MY_controller extends CI_controller {
    

    public $data = array();
    function __construct(){
        parent::__construct();
		$this->data['errors'] = array();
	
		/*$this->load->model('regulasi_model');
		$this->load->model('unduhan_model');*/
		//$this->load->model('parpol_m');
		if ($this->session->userdata('user_id') == NULL || $this->session->userdata('user_id') == '') {
			$this->session->sess_destroy();
			$this->session->set_flashdata('gagal', 'Anda harus login untuk masuk ke akun.');
			redirect(site_url());
		}
		/*if (($this->session->userdata('role') == 'peserta_didik' && $this->uri->segment(2) != 'peserta_didik') || ($this->session->userdata('role') == 'lembaga' && $this->uri->segment(2) != 'lembaga')) {
			$this->session->unset_userdata('user_id');
			$this->session->unset_userdata('nama');
			$this->session->unset_userdata('email');
			$this->session->unset_userdata('username');
			$this->session->unset_userdata('role');
			$this->session->unset_userdata('id_login');
			$this->session->sess_destroy();
			$this->session->set_flashdata('gagal', 'Anda harus login untuk masuk ke akun.');
			redirect(site_url('login'));
		}*/
    }


	//callback
	public function _dropdown_check($str) {
		if ($str == 'pilih') {
			$this->form_validation->set_message('_dropdown_check', '{field} harus dipilih !'); return false;
		}
		else {
			return true;
		}
	}

	
	protected function buat_pagging($total_data, $max, $base_url, $page_query_string)
	{
	    //pagination
	    $this->load->library('pagination');
	    $config['base_url'] = $base_url;
	    $config['total_rows'] = $total_data;
	    $config['per_page'] = $max;
	    $config['num_links'] = 4;
	    $config['use_page_numbers'] = TRUE;
	    $config['page_query_string'] = $page_query_string;

	    $config['first_link']       = 'First';
        $config['last_link']        = 'Last';
        $config['next_link']        = 'Next';
        $config['prev_link']        = 'Prev';
        $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
        $config['full_tag_close']   = '</ul></nav></div>';
        $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
        $config['num_tag_close']    = '</span></li>';
        $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
        $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
        $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
        $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['prev_tagl_close']  = '</span>Next</li>';
        $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
        $config['first_tagl_close'] = '</span></li>';
        $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
        $config['last_tagl_close']  = '</span></li>';
	    $this->pagination->initialize($config);
	    return $this->pagination->create_links();
	    //pagination end
	}


	public function array_from_post($fields) {
        $data = array();
        foreach ($fields as $field) {
            $data[$field] = $this->input->post($field, TRUE);
        }
        return $data;
    }

    //encryption
    public function hash($string){
        return hash('sha512', $string . config_item('encryption_key'));
    }
}