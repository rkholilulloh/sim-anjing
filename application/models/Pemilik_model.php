<?php

class Pemilik_Model extends CI_Model {

    protected $_table_name = 'pemilik';
    protected $_primary_key = 'id';
    protected $_order_by = 'nama';
    public $rules = array();
    protected $_timestamps = FALSE;
    public $data = array();
    

    function __construct(){

        parent::__construct();

    }

	//insert_data

	public function insert($data) {

		$this->db->set($data);

		$this->db->insert($this->_table_name);

		$id = $this->db->insert_id();

		return $id;

	}

	

	//update_data

	public function update($id = NULL, $data) {

		if($id != NULL) {

			$id = intval($id);

			$this->db->where($this->_primary_key, $id);

			$this->db->set($data);

			$update = $this->db->update($this->_table_name);

			return $update;

		}

	}

	

	//delete_data

    public function delete($id) {

		$id = intval($id);

        $this->db->where($this->_primary_key, $id);

        $delete = $this->db->delete($this->_table_name);

		return $delete;

	}

	

	//get_all_data

    public function get_all($id = NULL) {

		if ($id != NULL) {

            $id = intval($id);

            $this->db->where($this->_primary_key, $id);

		}

		

		$data = $this->db->get($this->_table_name);

		return $data->result();

	}

	

	//getdataby

    public function get_by($condition) {
		$this->db->select("pemilik.*, user.username, kelurahan.name AS 'nama_kelurahan', districts.name AS 'district'");
		$this->db->from($this->_table_name);
		$this->db->join('user','pemilik.id_user=user.id','left');
		$this->db->join("districts","pemilik.kecamatan=districts.id","left");
		$this->db->join("kelurahan","pemilik.kelurahan=kelurahan.id","left");
		$this->db->where($condition);

		$data = $this->db->get();

		return $data->result();

	}

}