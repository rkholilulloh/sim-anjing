<?php

class Login_Model extends CI_Model {

    protected $_table_name = 'user';
    protected $_primary_key = 'id';
    protected $_order_by = 'username';
    public $rules = array();
    protected $_timestamps = FALSE;
    public $data = array();
    

    function __construct(){

        parent::__construct();

    }

	//insert_data

	public function insert($data) {

		$this->db->set($data);

		$this->db->insert($this->_table_name);

		$id = $this->db->insert_id();

		return $id;

	}

	

	//update_data

	public function update($id = NULL, $data) {

		if($id != NULL) {

			$id = intval($id);

			$this->db->where($this->_primary_key, $id);

			$this->db->set($data);

			$update = $this->db->update($this->_table_name);

			return $update;

		}

	}

	

	//delete_data

    public function delete($id) {

		$id = intval($id);

        $this->db->where($this->_primary_key, $id);

        $delete = $this->db->delete($this->_table_name);

		return $delete;

	}

	

	//get_all_data

    public function get_all($id = NULL) {

		if ($id != NULL) {

            $id = intval($id);

            $this->db->where($this->_primary_key, $id);

		}

		

		$data = $this->db->get($this->_table_name);

		return $data;

	}

	

	//getdataby

    public function get_by($condition) {

		$this->db->where($condition);

		$data = $this->db->get($this->_table_name);

		return $data->result();

    }
    
    public function check_login($username, $password){

        $this->db->where("username='$username'");
        $this->db->where("password='$password'");

        $data = $this->db->get($this->_table_name);

        return $data;
    }

}