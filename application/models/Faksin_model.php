<?php

class Faksin_Model extends CI_Model {

    protected $_table_name = 'faksin';
    protected $_primary_key = 'id';
    protected $_order_by = 'nama';
    public $rules = array();
    protected $_timestamps = FALSE;
    public $data = array();
    

    function __construct(){

        parent::__construct();

    }

	//insert_data

	public function insert($data) {

		$this->db->set($data);

		$this->db->insert($this->_table_name);

		$id = $this->db->insert_id();

		return $id;

	}

	

	//update_data

	public function update($id = NULL, $data) {

		if($id != NULL) {

			$id = intval($id);

			$this->db->where($this->_primary_key, $id);

			$this->db->set($data);

			$update = $this->db->update($this->_table_name);

			return $update;

		}

	}

	

	//delete_data

    public function delete($id) {

		$id = intval($id);

        $this->db->where("id_anjing", $id);

        $delete = $this->db->delete($this->_table_name);

		return $delete;

	}

	

	//get_all_data

    public function get_all($id = NULL) {

		if ($id != NULL) {

            $id = intval($id);

            $this->db->where($this->_primary_key, $id);

		}

		

		$data = $this->db->get($this->_table_name);

		return $data;

	}

	

	//getdataby

    public function get_by($condition) {

		$this->db->select("faksin.*, jenis_vaksin.id AS 'id_jenis_vaksin', jenis_vaksin.jenis AS 'jenis_vaksin'");
		$this->db->where($condition);
		$this->db->join("jenis_vaksin","faksin.jenis=jenis_vaksin.id");

		$data = $this->db->get($this->_table_name);

		return $data->result();

	}

	public function insert_jenis_vaksin($jenis){
		$this->db->set(array("jenis"=>$jenis));

		$this->db->insert("jenis_vaksin");

		$id = $this->db->insert_id();

		return $id;
	}

	public function update_jenis_vaksin($id, $jenis){
		if($id != NULL) {

			$id = intval($id);

			$this->db->where("id", $id);

			$this->db->set(array("jenis"=>$jenis));

			$update = $this->db->update("jenis_vaksin");

			return $update;

		}
	}

	public function delete_jenis_vaksin($id) {

		$id = intval($id);

        $this->db->where("id", $id);

        $delete = $this->db->delete("jenis_vaksin");

		return $delete;

	}

}