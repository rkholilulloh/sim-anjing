<?php

class Anjing_Model extends CI_Model {

    protected $_table_name = 'anjing';
    protected $_primary_key = 'id';
    protected $_order_by = 'nama';
    public $rules = array();
    protected $_timestamps = FALSE;
    public $data = array();
    

    function __construct(){

        parent::__construct();

    }

	//insert_data

	public function insert($data) {

		$this->db->set($data);

		$this->db->insert($this->_table_name);

		$id = $this->db->insert_id();

		return $id;

	}

	

	//update_data

	public function update($id = NULL, $data) {

		if($id != NULL) {

			$id = intval($id);

			$this->db->where($this->_primary_key, $id);

			$this->db->set($data);

			$update = $this->db->update($this->_table_name);

			return $update;

		}

	}

	

	//delete_data

    public function delete($id) {

		$id = intval($id);

        $this->db->where($this->_primary_key, $id);

        $delete = $this->db->delete($this->_table_name);

		return $delete;

	}

	

	//get_all_data

    public function get_all($id = NULL) {

		if ($id != NULL) {

            $id = intval($id);

            $this->db->where($this->_primary_key, $id);

		}

		

		$data = $this->db->get($this->_table_name);

		return $data->result();

	}

	

	//getdataby

    public function get_by($condition) {

		$this->db->select("anjing.*, pemilik.nama AS 'nama_pemilik', pemilik.id AS 'id_pemilik', pemilik.koordinat, pemilik.alamat, regencies.name AS 'kabupaten', districts.name AS 'kecamatan', pemilik.no_hp, jenis_anjing.jenis, ras_anjing.ras");
		$this->db->from('anjing');
		$this->db->join("jenis_anjing","anjing.id_jenis_anjing=jenis_anjing.id","left");
		$this->db->join("ras_anjing","anjing.id_ras=ras_anjing.id","left");
		$this->db->join("pemilik","anjing.id_pemilik=pemilik.id");
		$this->db->join("regencies","pemilik.kabupaten=regencies.id","left");
		$this->db->join("districts","pemilik.kecamatan=districts.id","left");
		$this->db->where($condition);

		$data = $this->db->get();

		return $data->result();

	}

	public function insert_jenis_anjing($jenis){
		$this->db->set(array("jenis"=>$jenis));

		$this->db->insert("jenis_anjing");

		$id = $this->db->insert_id();

		return $id;
	}

	public function update_jenis_anjing($id, $jenis){
		if($id != NULL) {

			$id = intval($id);

			$this->db->where("id", $id);

			$this->db->set(array("jenis"=>$jenis));

			$update = $this->db->update("jenis_anjing");

			return $update;

		}
	}

	public function delete_jenis_anjing($id) {

		$id = intval($id);

        $this->db->where("id", $id);

        $delete = $this->db->delete("jenis_anjing");

		return $delete;

	}

	public function insert_ras_anjing($ras){
		$this->db->set(array("ras"=>$ras));

		$this->db->insert("ras_anjing");

		$id = $this->db->insert_id();

		return $id;
	}

	public function update_ras_anjing($id, $ras){
		if($id != NULL) {

			$id = intval($id);

			$this->db->where("id", $id);

			$this->db->set(array("ras"=>$ras));

			$update = $this->db->update("ras_anjing");

			return $update;

		}
	}

	public function delete_ras_anjing($id) {

		$id = intval($id);

        $this->db->where("id", $id);

        $delete = $this->db->delete("ras_anjing");

		return $delete;

	}

}