<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenis extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!$this->session->has_userdata('user')){
            redirect('login');
        }

        if($this->session->userdata('role')!='admin'){
            show_error("Anda bukan Admin",403);
        }
    }

    public function index(){
        redirect('pengaturan/jenis');
    }

    public function insert(){
        $this->load->model('Anjing_model');

        $jenis = $this->input->post('jenis_anjing');

        $this->form_validation->set_rules('jenis_anjing','Jenis Anjing','required');

        if(!$this->form_validation->run()){
            $this->session->set_flashdata('crud_error',validation_errors());
            redirect('pengaturan/jenis');
        }

        $inserted_id = $this->Anjing_model->insert_jenis_anjing($jenis);

        if($inserted_id > 0){
            $this->session->set_flashdata('crud_success','Data berhasil ditambahkan');
            redirect('pengaturan/jenis');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
            redirect('pengaturan/jenis');
        }
    }

    public function update($id){
        if($id == null){
            show_404();
        }

        $this->load->model('Anjing_model');

        $jenis = $this->input->post('jenis_anjing');
        $this->form_validation->set_rules('jenis_anjing','Jenis Anjing','required');

        if(!$this->form_validation->run()){
            $this->session->set_flashdata('crud_error',validation_errors());
            redirect('pengaturan/jenis');
        }

        if($this->Anjing_model->update_jenis_anjing($id,$jenis)){
            $this->session->set_flashdata('crud_success','Data berhasil diupdate');
            redirect('pengaturan/jenis');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
            redirect('pengaturan/jenis');
        }
    }

    public function delete($id){
        $this->load->model('Anjing_model');

        if($this->Anjing_model->delete_jenis_anjing($id)){
            $this->session->set_flashdata('crud_success','Data berhasil dihapus');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
        }
        redirect('pengaturan/jenis');
    }

}