<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ras extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!$this->session->has_userdata('user')){
            redirect('login');
        }

        if($this->session->userdata('role')!='admin'){
            show_error("Anda bukan Admin",403);
        }
    }

    public function index(){
        redirect('pengaturan/ras');
    }

    public function insert(){
        $this->load->model('Anjing_model');

        $ras = $this->input->post('ras');

        $this->form_validation->set_rules('ras','Ras Anjing','required');

        if(!$this->form_validation->run()){
            $this->session->set_flashdata('crud_error',validation_errors());
            redirect('pengaturan/ras');
        }

        $inserted_id = $this->Anjing_model->insert_ras_anjing($ras);

        if($inserted_id > 0){
            $this->session->set_flashdata('crud_success','Data berhasil ditambahkan');
            redirect('pengaturan/ras');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
            redirect('pengaturan/ras');
        }
    }

    public function update($id){
        if($id == null){
            show_404();
        }

        $this->load->model('Anjing_model');

        $ras = $this->input->post('ras');
        $this->form_validation->set_rules('ras','Ras Anjing','required');

        if(!$this->form_validation->run()){
            $this->session->set_flashdata('crud_error',validation_errors());
            redirect('pengaturan/ras');
        }

        if($this->Anjing_model->update_ras_anjing($id,$ras)){
            $this->session->set_flashdata('crud_success','Data berhasil diupdate');
            redirect('pengaturan/ras');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
            redirect('pengaturan/ras');
        }
    }

    public function delete($id){
        $this->load->model('Anjing_model');

        if($this->Anjing_model->delete_ras_anjing($id)){
            $this->session->set_flashdata('crud_success','Data berhasil dihapus');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
        }
        redirect('pengaturan/ras');
    }

}