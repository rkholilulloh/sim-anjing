<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vaksin extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!$this->session->has_userdata('user')){
            redirect('login');
        }

        if($this->session->userdata('role')!='admin'){
            show_error("Anda bukan Admin",403);
        }
    }

    public function index(){
        redirect('pengaturan/vaksin');
    }

    public function insert(){
        $this->load->model('Faksin_model');

        $jenis = $this->input->post('jenis_vaksin');

        $this->form_validation->set_rules('jenis_vaksin','Jenis Vaksin','required');

        if(!$this->form_validation->run()){
            $this->session->set_flashdata('crud_error',validation_errors());
            redirect('pengaturan/vaksin');
        }

        $inserted_id = $this->Faksin_model->insert_jenis_vaksin($jenis);

        if($inserted_id > 0){
            $this->session->set_flashdata('crud_success','Data berhasil ditambahkan');
            redirect('pengaturan/vaksin');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
            redirect('pengaturan/vaksin');
        }
    }

    public function update($id){
        if($id == null){
            show_404();
        }

        $this->load->model('Faksin_model');

        $jenis = $this->input->post('jenis_vaksin');
        $this->form_validation->set_rules('jenis_vaksin','Jenis Vaksin','required');

        if(!$this->form_validation->run()){
            $this->session->set_flashdata('crud_error',validation_errors());
            redirect('pengaturan/vaksin');
        }

        if($this->Faksin_model->update_jenis_vaksin($id,$jenis)){
            $this->session->set_flashdata('crud_success','Data berhasil diupdate');
            redirect('pengaturan/vaksin');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
            redirect('pengaturan/vaksin');
        }
    }

    public function delete($id){
        $this->load->model('Faksin_model');

        if($this->Faksin_model->delete_jenis_vaksin($id)){
            $this->session->set_flashdata('crud_success','Data berhasil dihapus');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
        }
        redirect('pengaturan/vaksin');
    }

}