<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pengaturan extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!$this->session->has_userdata('user')){
            redirect('login');
        }
    }

    public function index(){
        redirect('pengaturan/password');
    }

    public function password(){
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('pengaturan');
        $this->load->view('template/footer');
    }

    public function jenis(){
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('jenis_anjing');
        $this->load->view('template/footer');
    }

    public function ras(){
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('ras_anjing');
        $this->load->view('template/footer');
    }

    public function vaksin(){
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('jenis_vaksin');
        $this->load->view('template/footer');
    }

    public function ganti_password(){

        $this->load->model('Login_model');

        $id = $this->session->userdata('id_user');

        $old_password = hash('sha256', "yYJol5PDPFtxiO6Xc49b".$this->input->post('old_password'));
        $password = hash('sha256', "yYJol5PDPFtxiO6Xc49b".$this->input->post('password'));
        $confirm_password = hash('sha256', "yYJol5PDPFtxiO6Xc49b".$this->input->post('confirm_password'));

        $this->form_validation->set_rules('old_password','Password lama','required');
        $this->form_validation->set_rules('password','Password','required');
        $this->form_validation->set_rules('confirm_password','Ulangi password','required|matches[password]');

        if(!$this->form_validation->run()){
            $this->session->set_flashdata('crud_error',validation_errors());
            redirect('pengaturan');
        }

        if($old_password !== $this->Login_model->get_by("id='$id'")[0]->password){
            $this->session->set_flashdata('crud_error', "Password lama salah");
            redirect('pengaturan');
        }

        if($this->Login_model->update($id, array("password"=>$password))){
            $this->session->set_flashdata('crud_success', "Password berhasil diperbarui");
            redirect('pengaturan');
        } else {
            $this->session->set_flashdata('crud_error', $this->db->error());
            redirect('pengaturan');
        }
    }

}
