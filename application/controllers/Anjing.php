<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Anjing extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!$this->session->has_userdata('user')){
            redirect('login');
        }
    }

    public function index(){
        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('anjing');
        $this->load->view('template/footer');
    }

    public function tambah(){
        $data['type'] = 'Tambah';

        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('input_anjing', $data);
        $this->load->view('template/footer');
    }

    public function edit($id){
        $this->load->model('Anjing_model');
        $this->load->model('Faksin_model');

        $data['records'] = $this->Anjing_model->get_by("anjing.id='$id'");
        if(count($data['records']) < 1){
            show_404();
        } else {
            $data['records'] = $data['records'][0];
            $data['faksin'] = $this->Faksin_model->get_by("faksin.id_anjing='$id'");
        }
        $data['type'] = 'Edit';

        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('input_anjing', $data);
        $this->load->view('template/footer');
    }

    public function insert(){
        $this->load->model('Anjing_model');
        $this->load->model('Faksin_model');

        $nama = $this->input->post('nama');
        $id_pemilik = $this->input->post('id_pemilik');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $ras = $this->input->post('id_ras');

        $unique_id = base_convert(microtime(true), 10, 36);

        $nama_faksin = $this->input->post('nama_faksin');
        $jenis_faksin = $this->input->post('jenis_faksin');

        if($this->session->userdata('role')!='admin'){
            $this->load->model('Pemilik_model');
            $id_user = $this->session->userdata('id_user');
            $id_pemilik = $this->Pemilik_model->get_by("id_user='$id_user'")[0]->id;
        }

        $this->form_validation->set_rules('nama','Nama Anjing','required');
        if($this->session->userdata('role')=='admin'){
            $this->form_validation->set_rules('id_pemilik','Pemilik Anjing','required');
        }
        $this->form_validation->set_rules('tgl_lahir','Tanggal Lahir','required');
        $this->form_validation->set_rules('id_ras','Ras Anjing','required');
        if(empty($_FILES['foto']['name']))
        {
            $this->form_validation->set_rules('foto','Foto Anjing','required');
        }

        if(!$this->form_validation->run()){
            $this->session->set_flashdata('crud_error',validation_errors());
            redirect('anjing/tambah');
        }

        $config['upload_path']          = './uploads/';
		$config['allowed_types']        = 'gif|jpg|png';
        $config['max_size'] 			= 2048;
        $config['encrypt_name']         = TRUE;
        
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('foto')){
            $this->session->set_flashdata('crud_error',$this->upload->display_errors());
            redirect('anjing/tambah');
        } else {
            $foto_url = "uploads/".$this->upload->data('file_name');
        }

        $inserted_id = $this->Anjing_model->insert(array(
            "nama"=>$nama,
            "id_pemilik"=>$id_pemilik,
            "jenis_kelamin"=>$jenis_kelamin,
            "tgl_lahir"=>$tgl_lahir,
            "id_ras"=>$ras,
            "unique_id"=>$unique_id,
            "foto"=>$foto_url
        ));

        if($inserted_id >= 0){
            for($i=0; $i < count($nama_faksin); $i++){
                if(empty($nama_faksin[$i])){
                    continue;
                }
                $this->Faksin_model->insert(
                    array(
                        "nama"=>$nama_faksin[$i],
                        "jenis"=>$jenis_faksin[$i],
                        "id_anjing"=>$inserted_id
                    )
                );
            }

            //Generate ID Anjing
            $this->load->model('Pemilik_model');
            $data_pemilik = $this->Pemilik_model->get_by("pemilik.id='$id_pemilik'")[0];
            $id_kecamatan = $data_pemilik->kecamatan;
            $id_kelurahan = str_pad($data_pemilik->kelurahan, 2, '0', STR_PAD_LEFT);
            $id_ras = str_pad($ras, 2, '0', STR_PAD_LEFT);
            $real_id = str_pad($inserted_id, 3, '0', STR_PAD_LEFT);

            $id_anjing = $id_kecamatan.$id_kelurahan.$id_ras.$real_id;
            $this->Anjing_model->update($inserted_id,
                array(
                    "id_anjing"=>$id_anjing
                )
            );
    
            $this->session->set_flashdata('crud_success','Data berhasil ditambahkan');
            redirect('anjing');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
            redirect('anjing/tambah');
        }
    }

    public function update($id){
        if($id == null){
            show_404();
        }

        $this->load->model('Anjing_model');
        $this->load->model('Faksin_model');

        $nama = $this->input->post('nama');
        $id_pemilik = $this->input->post('id_pemilik');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $tgl_lahir = $this->input->post('tgl_lahir');
        $ras = $this->input->post('id_ras');

        $nama_faksin = $this->input->post('nama_faksin');
        $jenis_faksin = $this->input->post('jenis_faksin');

        if($this->session->userdata('role')!='admin'){
            $this->load->model('Pemilik_model');
            $id_user = $this->session->userdata('id_user');
            $id_pemilik = $this->Pemilik_model->get_by("id_user='$id_user'")[0]->id;
        }

        $this->form_validation->set_rules('nama','Nama Anjing','required');
        if($this->session->userdata('role')=='admin'){
            $this->form_validation->set_rules('id_pemilik','Pemilik Anjing','required');
        }
        $this->form_validation->set_rules('tgl_lahir','Tanggal Lahir','required');
        $this->form_validation->set_rules('id_ras','Ras Anjing','required');

        if(!$this->form_validation->run()){
            $this->session->set_flashdata('crud_error',validation_errors());
            redirect('anjing/edit/'.$id);
        }

        $data = array(
            "nama"=>$nama,
            "id_pemilik"=>$id_pemilik,
            "jenis_kelamin"=>$jenis_kelamin,
            "tgl_lahir"=>$tgl_lahir,
            "id_ras"=>$ras
        );

        if(!empty($_FILES['foto']['name'])){
            $config['upload_path']          = './uploads/';
            $config['allowed_types']        = 'gif|jpg|png';
            $config['max_size'] 			= 2048;
            $config['encrypt_name']         = TRUE;
            
            $this->load->library('upload', $config);

            if(!$this->upload->do_upload('foto')){
                $this->session->set_flashdata('crud_error',$this->upload->display_errors());
                redirect('anjing/edit/'.$id);
            } else {
                $data['foto'] = "uploads/".$this->upload->data('file_name');
            }
        }

        $updated = $this->Anjing_model->update($id, $data);

        if($updated){
            $this->Faksin_model->delete($id);
            for($i=0; $i < count($nama_faksin); $i++){
                if(empty($nama_faksin[$i])){
                    continue;
                }
                $this->Faksin_model->insert(
                    array(
                        "nama"=>$nama_faksin[$i],
                        "jenis"=>$jenis_faksin[$i],
                        "id_anjing"=>$id
                    )
                );
            }
    
            $this->session->set_flashdata('crud_success','Data berhasil diupdate');
            redirect('anjing');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
            redirect('anjing/edit/'.$id);
        }
    }

    public function delete($id){
        $this->load->model('Anjing_model');
        $this->load->model('Faksin_model');

        if($this->Anjing_model->delete($id)){
            $this->Faksin_model->delete($id);
            $this->session->set_flashdata('crud_success','Data berhasil dihapus');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
        }
        redirect('anjing');
    }

}
