<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!$this->session->has_userdata('user')){
            redirect('login');
        }
    }

    public function index(){
        $this->load->model('Pemilik_model');
        $data['anjing_total'] = $this->db->count_all_results("anjing");
        $data['pemilik_total'] = $this->db->count_all_results("pemilik");

        $data['pemilik'] = $this->Pemilik_model->get_all();

        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('dashboard', $data);
        $this->load->view('template/footer');
    }

}
