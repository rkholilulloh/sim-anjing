<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function index(){
        if($this->session->has_userdata('user')){
            redirect('/');
        }

        $this->load->view('login');
    }

    public function auth($arg="in"){
        if($arg=="in"){
            if($this->session->has_userdata('user')){
				redirect('/');
            }
            
            $this->load->model("Login_model");

            $username = $this->input->post('username');
            $password = hash('sha256', "yYJol5PDPFtxiO6Xc49b".$this->input->post('password'));

            $query = $this->Login_model->check_login($username, $password);

            if($query->num_rows()==1){
                $data = $query->result()[0];
                $this->session->set_userdata('user', $data->username);
                $this->session->set_userdata('id_user', $data->id);
                $this->session->set_userdata('role', $data->role);
                redirect('/');
            } else {
                $this->session->set_flashdata('login_error',"Username atau Password salah");
                redirect('login');
            }
        } else {
            $this->session->unset_userdata('user');
            $this->session->unset_userdata('id_user');
            $this->session->unset_userdata('role');
            $this->session->sess_destroy();

            redirect('login');
        }
    }

}
