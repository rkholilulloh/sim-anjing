<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class View extends CI_Controller {

    public function index($unique_id){

        $this->load->model('Anjing_model');

        $data['anjing'] = $this->Anjing_model->get_by("unique_id='$unique_id'");

        if(count($data['anjing']) == 0){
            show_404();
        } else {
            $data['anjing'] = $data['anjing'][0];
            $this->load->view('view_anjing', $data);
        }
    }

}