<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {
    
	public function __construct(){
		parent::__construct();
		header('Content-Type: application/json');

		if(!$this->session->has_userdata('user')){
			echo json_encode(array("error"=>"Forbidden"));
			exit;
		}
	}

	protected function filter_xss($data){
		$result = array();
		foreach($data as $key=>$value){
			$result[$key] = $this->security->xss_clean($value);
		}
		return $result;
	}

	public function view_detail_anjing($id){
		$this->db->select("pemilik.nama AS 'nama_pemilik', CONCAT(pemilik.alamat, ', ', kelurahan.name, ', ', districts.name, ', ', regencies.name) AS 'alamat', anjing.id_anjing, anjing.nama, anjing.jenis_kelamin, anjing.tgl_lahir, CONCAT(FLOOR(DATEDIFF(CURRENT_DATE, anjing.tgl_lahir)/365), ' tahun'), ras_anjing.ras, anjing.unique_id, GROUP_CONCAT(DISTINCT CONCAT(faksin.nama, ' (', jenis_vaksin.jenis, ')') SEPARATOR ', '), anjing.foto");
		$this->db->from("anjing");
		$this->db->join("pemilik","anjing.id_pemilik=pemilik.id");
		$this->db->join("faksin","anjing.id=faksin.id_anjing","left");
		$this->db->join("ras_anjing","anjing.id_ras=ras_anjing.id","left");
		$this->db->join("regencies","pemilik.kabupaten=regencies.id","left");
		$this->db->join("districts","pemilik.kecamatan=districts.id","left");
		$this->db->join("kelurahan","pemilik.kelurahan=kelurahan.id","left");
		$this->db->join("jenis_vaksin","faksin.jenis=jenis_vaksin.id","left");
		$this->db->group_by("anjing.id");
		$this->db->where("anjing.id",$id);
		$query = $this->db->get();

		if($query->num_rows() > 0){
			echo json_encode(
				array(
					"result"=>$query->result()[0]
				)
			);
		} else {
			echo json_encode(
				array("status"=>"error","msg"=>"ID tidak ditemukan")
			);
		}
	}

	public function view_detail_pemilik($id){
		$this->db->select("pemilik.nama, pemilik.jenis_kelamin, pemilik.alamat, kelurahan.name AS 'kelurahan', districts.name AS 'kecamatan', pemilik.nik, pemilik.no_hp, pemilik.koordinat, user.username");
		$this->db->from("pemilik");
		$this->db->join("user","pemilik.id_user=user.id","left");
		$this->db->join("kelurahan","pemilik.kelurahan=kelurahan.id","left");
		$this->db->join("regencies","pemilik.kabupaten=regencies.id","left");
		$this->db->join("districts","pemilik.kecamatan=districts.id","left");
		$this->db->where("pemilik.id",$id);
		$query = $this->db->get();

		if($query->num_rows() > 0){
			echo json_encode(
				array(
					"result"=>$query->result()[0]
				)
			);
		} else {
			echo json_encode(
				array("status"=>"error","msg"=>"ID tidak ditemukan")
			);
		}
	}

	public function get_pemilik_by_location(){

		$kecamatan = $this->input->get('kecamatan');
		$kelurahan = $this->input->get('kelurahan');

		$this->db->select("pemilik.id, pemilik.nama, pemilik.jenis_kelamin, pemilik.alamat, kelurahan.name AS 'kelurahan', districts.name AS 'kecamatan', pemilik.nik, pemilik.no_hp, pemilik.koordinat, user.username");
		$this->db->from("pemilik");
		$this->db->join("user","pemilik.id_user=user.id","left");
		$this->db->join("districts","pemilik.kecamatan=districts.id","left");
		$this->db->join("kelurahan","pemilik.kelurahan=kelurahan.id","left");
		if(!empty($kecamatan) and $kecamatan != "all"){
			$this->db->where("districts.id='$kecamatan'");
		}
		if(!empty($kelurahan) and $kelurahan != "all"){
			$this->db->where("kelurahan.id='$kelurahan'");
		}
		$query = $this->db->get();
		
		echo json_encode(
			array(
				"result"=>$query->result()
			)
		);
	}

	public function get_jenis_anjing(){
		$q = $this->input->get('q');

		$this->db->select("*");
		$this->db->from("jenis_anjing");
		$this->db->like("jenis",$q);
		$this->db->order_by("jenis");
		$query = $this->db->get();

		$data = array(
			"result"=>$query->result()
		);

		echo json_encode($data);
	}

	public function get_ras_anjing(){
		$q = $this->input->get('q');

		$this->db->select("*");
		$this->db->from("ras_anjing");
		$this->db->like("ras",$q);
		$this->db->order_by("ras");
		$query = $this->db->get();

		$data = array(
			"result"=>$query->result()
		);

		echo json_encode($data);
	}

	public function get_kelurahan(){
		$q = $this->input->get('q');
		$reg = $this->input->get('reg');

		$this->db->select("id, name");
		$this->db->from("kelurahan");
		$this->db->like("name",$q);
		$this->db->where("district_id",$reg);
		$this->db->order_by("name");
		$query = $this->db->get();

		$data = array(
			"result"=>$query->result()
		);

		echo json_encode($data);
	}

	public function get_kecamatan(){
		$q = $this->input->get('q');

		$this->db->select("id, name");
		$this->db->from("districts");
		$this->db->like("name",$q);
		$this->db->where("regency_id","3372");
		$this->db->order_by("name");
		$query = $this->db->get();

		$data = array(
			"result"=>$query->result()
		);

		echo json_encode($data);
	}

	public function get_kabupaten(){
		$q = $this->input->get('q');

		$this->db->select("id, name");
		$this->db->from("regencies");
		$this->db->like("name",$q);
		$this->db->order_by("name");
		$query = $this->db->get();

		$data = array(
			"result"=>$query->result()
		);

		echo json_encode($data);
	}

	public function get_jenis_vaksin(){

		$this->db->select("*");
		$this->db->from("jenis_vaksin");
		$this->db->order_by("jenis");
		$query = $this->db->get();

		$data = array(
			"result"=>$query->result()
		);

		echo json_encode($data);
	}

	public function select_anjing(){

		$table_name = "anjing";

		$draw = $this->input->get('draw');
		$length = $this->input->get('length');
		$start = $this->input->get('start');
		$columns = $this->input->get('columns');
		$search = isset($this->input->get('search')['value']) ? $this->input->get('search')['value'] : null;
		$order = $this->input->get('order');

		$total = $this->db->count_all_results($table_name);

		if($search != ""){
			$this->db->or_like(array('anjing.nama'=>$search,'anjing.jenis_kelamin'=>$search,'anjing.tgl_lahir'=>$search,'jenis_anjing.jenis'=>$search,'ras_anjing.ras'=>$search,'pemilik.nama'=>$search));
		}

		$this->db->select("anjing.*, pemilik.nama AS 'nama_pemilik', jenis_anjing.jenis, ras_anjing.ras");
		$this->db->from($table_name);
		$this->db->join("pemilik","anjing.id_pemilik=pemilik.id","left");
		$this->db->join("jenis_anjing","anjing.id_jenis_anjing=jenis_anjing.id","left");
		$this->db->join("ras_anjing","anjing.id_ras=ras_anjing.id","left");
		$this->db->limit($length,$start);
		for($i=0;$i < count($order);$i++){
			$this->db->order_by($columns[$order[$i]['column']]['data'],$order[$i]['dir']);
		}

		if($this->session->userdata('role')!='admin'){
			$this->db->where("pemilik.id_user",$this->session->userdata('id_user'));
		}
		$query = $this->db->get();
		$total_filtered = $query->num_rows();

		$output = array(
			"draw"=>$draw,
			"recordsTotal"=>$total,
			"recordsFiltered"=>$total_filtered,
			"data"=>array()
		);

		foreach ($query->result() as $d){
			$a = $this->filter_xss($d);
			$a['DT_RowId'] = 'row_'.$d->id;
			$output['data'][] = $a;
		}

		echo json_encode($output);
	}

	public function get_id_pemilik(){
		$q = $this->input->get('q');

		$this->db->select("id, nama");
		$this->db->from("pemilik");
		$this->db->like("nama",$q);
		$this->db->limit(20,0);
		$query = $this->db->get();

		$data = array(
			"result"=>$query->result()
		);

		echo json_encode($data);
	}

	public function select_pemilik(){

		if($this->session->userdata('role')!='admin'){
			echo json_encode(array("error"=>"Forbidden"));
			exit;
		}

		$table_name = "pemilik";

		$draw = $this->input->get('draw');
		$length = $this->input->get('length');
		$start = $this->input->get('start');
		$columns = $this->input->get('columns');
		$search = isset($this->input->get('search')['value']) ? $this->input->get('search')['value'] : null;
		$order = $this->input->get('order');

		$total = $this->db->count_all_results($table_name);

		if($search != ""){
			$this->db->or_like(array('pemilik.nama'=>$search,'pemilik.jenis_kelamin'=>$search,'pemilik.alamat'=>$search,'districts.name'=>$search,'regencies.name'=>$search,'pemilik.no_hp'=>$search,'pemilik.nik'=>$search));
		}

		$this->db->select("pemilik.*, kelurahan.name AS 'nama_kelurahan', districts.name AS 'district'");
		$this->db->from($table_name);
		$this->db->limit($length,$start);
		for($i=0;$i < count($order);$i++){
			$this->db->order_by($columns[$order[$i]['column']]['data'],$order[$i]['dir']);
		}
		$this->db->join("districts","pemilik.kecamatan=districts.id","left");
		$this->db->join("kelurahan","pemilik.kelurahan=kelurahan.id","left");
		$query = $this->db->get();
		$total_filtered = $query->num_rows();

		$output = array(
			"draw"=>$draw,
			"recordsTotal"=>$total,
			"recordsFiltered"=>$total_filtered,
			"data"=>array()
		);

		foreach ($query->result() as $d) {
			$a = $this->filter_xss($d);
			$a['DT_RowId'] = 'row_'.$d->id;
			$output['data'][] = $a;
		}

		echo json_encode($output);
	}

	public function select_jenis_anjing(){

		if($this->session->userdata('role')!='admin'){
			echo json_encode(array("error"=>"Forbidden"));
			exit;
		}

		$table_name = "jenis_anjing";

		$draw = $this->input->get('draw');
		$length = $this->input->get('length');
		$start = $this->input->get('start');
		$columns = $this->input->get('columns');
		$search = isset($this->input->get('search')['value']) ? $this->input->get('search')['value'] : null;
		$order = $this->input->get('order');

		$total = $this->db->count_all_results($table_name);

		if($search != ""){
			$this->db->or_like(array('jenis'=>$search));
		}

		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->limit($length,$start);
		for($i=0;$i < count($order);$i++){
			$this->db->order_by($columns[$order[$i]['column']]['data'],$order[$i]['dir']);
		}
		$query = $this->db->get();
		$total_filtered = $query->num_rows();

		$output = array(
			"draw"=>$draw,
			"recordsTotal"=>$total,
			"recordsFiltered"=>$total_filtered,
			"data"=>array()
		);

		foreach ($query->result() as $d) {
			$a = $this->filter_xss($d);
			$a['DT_RowId'] = 'row_'.$d->id;
			$output['data'][] = $a;
		}

		echo json_encode($output);
	}

	public function select_ras_anjing(){

		if($this->session->userdata('role')!='admin'){
			echo json_encode(array("error"=>"Forbidden"));
			exit;
		}

		$table_name = "ras_anjing";

		$draw = $this->input->get('draw');
		$length = $this->input->get('length');
		$start = $this->input->get('start');
		$columns = $this->input->get('columns');
		$search = isset($this->input->get('search')['value']) ? $this->input->get('search')['value'] : null;
		$order = $this->input->get('order');

		$total = $this->db->count_all_results($table_name);

		if($search != ""){
			$this->db->or_like(array('ras'=>$search));
		}

		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->limit($length,$start);
		for($i=0;$i < count($order);$i++){
			$this->db->order_by($columns[$order[$i]['column']]['data'],$order[$i]['dir']);
		}
		$query = $this->db->get();
		$total_filtered = $query->num_rows();

		$output = array(
			"draw"=>$draw,
			"recordsTotal"=>$total,
			"recordsFiltered"=>$total_filtered,
			"data"=>array()
		);

		foreach ($query->result() as $d) {
			$a = $this->filter_xss($d);
			$a['DT_RowId'] = 'row_'.$d->id;
			$output['data'][] = $a;
		}

		echo json_encode($output);
	}

	public function select_jenis_vaksin(){

		if($this->session->userdata('role')!='admin'){
			echo json_encode(array("error"=>"Forbidden"));
			exit;
		}

		$table_name = "jenis_vaksin";

		$draw = $this->input->get('draw');
		$length = $this->input->get('length');
		$start = $this->input->get('start');
		$columns = $this->input->get('columns');
		$search = isset($this->input->get('search')['value']) ? $this->input->get('search')['value'] : null;
		$order = $this->input->get('order');

		$total = $this->db->count_all_results($table_name);

		if($search != ""){
			$this->db->or_like(array('ras'=>$search));
		}

		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->limit($length,$start);
		for($i=0;$i < count($order);$i++){
			$this->db->order_by($columns[$order[$i]['column']]['data'],$order[$i]['dir']);
		}
		$query = $this->db->get();
		$total_filtered = $query->num_rows();

		$output = array(
			"draw"=>$draw,
			"recordsTotal"=>$total,
			"recordsFiltered"=>$total_filtered,
			"data"=>array()
		);

		foreach ($query->result() as $d) {
			$a = $this->filter_xss($d);
			$a['DT_RowId'] = 'row_'.$d->id;
			$output['data'][] = $a;
		}

		echo json_encode($output);
	}
}
