<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pemilik extends CI_Controller {

    public function __construct(){
        parent::__construct();
        if(!$this->session->has_userdata('user')){
            redirect('login');
        }

        if($this->session->userdata('role')!='admin'){
            show_error("Anda bukan Admin",403);
        }
    }

    public function index(){

        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('pemilik');
        $this->load->view('template/footer');
    }

    public function tambah(){
        $data['type'] = 'Tambah';
        $data['map'] = true;

        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('input_pemilik', $data);
        $this->load->view('template/footer');
    }

    public function edit($id){
        $this->load->model('Pemilik_model');

        $data['records'] = $this->Pemilik_model->get_by("pemilik.id='$id'");
        if(count($data['records']) < 1){
            show_404();
        } else {
            $data['records'] = $data['records'][0];
            $coord = explode(',', $data['records']->koordinat);
            if(count($coord)==2){
                $data['lat'] = $coord[0];
                $data['lng'] = $coord[1];
            } else {
                $data['lat'] = "-7.583";
                $data['lng'] = "110.833";
            }
        }
        $data['type'] = 'Edit';
        $data['map'] = true;

        $this->load->view('template/header');
        $this->load->view('template/sidebar');
        $this->load->view('input_pemilik', $data);
        $this->load->view('template/footer');
    }

    public function insert(){
        $this->load->model('Pemilik_model');
        $this->load->model('Login_model');

        $nama = $this->input->post('nama');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $alamat = $this->input->post('alamat');
        $asal = $this->input->post('asal');
        $kecamatan = $this->input->post('kecamatan');
        $kelurahan = $this->input->post('kelurahan');
        $nik = $this->input->post('nik');
        $no_hp = $this->input->post('no_hp');
        $lat = $this->input->post('lat');
        $lng = $this->input->post('lng');

        $username = $this->input->post('username');
        $password = hash('sha256', "yYJol5PDPFtxiO6Xc49b".$this->input->post('password'));

        if(!preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $username)){
            $this->session->set_flashdata('crud_error',"Username hanya boleh mengandung huruf, angka, atau underscore");
            redirect('pemilik/tambah');
        }

        $this->form_validation->set_rules('nama','Nama pemilik','required');
        $this->form_validation->set_rules('alamat','Alamat','required');
        $this->form_validation->set_rules('kecamatan','Kecamatan','required');
        $this->form_validation->set_rules('kelurahan','Kelurahan','required');
        $this->form_validation->set_rules('nik','NIK','required');
        $this->form_validation->set_rules('no_hp','No. HP','required');
        $this->form_validation->set_rules('username','Username','required|is_unique[user.username]');
        $this->form_validation->set_rules('password','Password','required');

        if(!$this->form_validation->run()){
            $this->session->set_flashdata('crud_error',validation_errors());
            redirect('pemilik/tambah');
        }

        $inserted_id = $this->Login_model->insert(
            array(
                "username"=>$username,
                "password"=>$password
            )
        );

        if($inserted_id > 0){

            $this->Pemilik_model->insert(array(
                "nama"=>$nama,
                "jenis_kelamin"=>$jenis_kelamin,
                "alamat"=>$alamat,
                "kecamatan"=>$kecamatan,
                "kelurahan"=>$kelurahan,
                "asal"=>$asal,
                "nik"=>$nik,
                "no_hp"=>$no_hp,
                "koordinat"=>$lat.",".$lng,
                "id_user"=>$inserted_id
            ));
    
            $this->session->set_flashdata('crud_success','Data berhasil ditambahkan');
            redirect('pemilik');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
            redirect('pemilik/tambah');
        }
    }

    public function update($id){
        if($id == null){
            show_404();
        }

        $this->load->model('Pemilik_model');
        $this->load->model('Login_model');

        $nama = $this->input->post('nama');
        $jenis_kelamin = $this->input->post('jenis_kelamin');
        $alamat = $this->input->post('alamat');
        $kecamatan = $this->input->post('kecamatan');
        $kelurahan = $this->input->post('kelurahan');
        $asal = $this->input->post('asal');
        $nik = $this->input->post('nik');
        $no_hp = $this->input->post('no_hp');
        $lat = $this->input->post('lat');
        $lng = $this->input->post('lng');

        $username = $this->input->post('username');
        if(!empty($this->input->post('password'))){
            $password = hash('sha256', "yYJol5PDPFtxiO6Xc49b".$this->input->post('password'));
        } else {
            $password = false;
        }

        if(!preg_match('/^[a-zA-Z]+[a-zA-Z0-9._]+$/', $username)){
            $this->session->set_flashdata('crud_error',"Username hanya boleh mengandung huruf, angka, atau underscore");
            redirect('pemilik/tambah');
        }

        $id_user = $this->Pemilik_model->get_by("pemilik.id='$id'")[0]->id_user;
        $original_username = $this->Login_model->get_by("id='$id_user'")[0]->username;

        $this->form_validation->set_rules('nama','Nama pemilik','required');
        $this->form_validation->set_rules('alamat','Alamat','required');
        $this->form_validation->set_rules('kecamatan','Kecamatan','required');
        $this->form_validation->set_rules('kelurahan','Kelurahan','required');
        $this->form_validation->set_rules('nik','NIK','required');
        $this->form_validation->set_rules('no_hp','No. HP','required');
        if($username != $original_username){
            $this->form_validation->set_rules('username','Username','required|is_unique[user.username]');
        }

        if(!$this->form_validation->run()){
            $this->session->set_flashdata('crud_error',validation_errors());
            redirect('pemilik/edit/'.$id);
        }

        $updated = $this->Pemilik_model->update($id, array(
            "nama"=>$nama,
            "jenis_kelamin"=>$jenis_kelamin,
            "alamat"=>$alamat,
            "kecamatan"=>$kecamatan,
            "kelurahan"=>$kelurahan,
            "asal"=>$asal,
            "nik"=>$nik,
            "no_hp"=>$no_hp,
            "koordinat"=>$lat.",".$lng
        ));

        if($updated){

            if($password){
                $data = array("username"=>$username,"password"=>$password);
            } else {
                $data = array("username"=>$username);
            }

            $this->Login_model->update($id_user, $data);
            $this->session->set_flashdata('crud_success','Data berhasil diupdate');
            redirect('pemilik');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
            redirect('pemilik/edit/'.$id);
        }
    }

    public function delete($id){
        $this->load->model('Pemilik_model');

        if($this->Pemilik_model->delete($id)){
            $this->session->set_flashdata('crud_success','Data berhasil dihapus');
        } else {
            $this->session->set_flashdata('crud_error',$this->db->error());
        }
        redirect('pemilik');
    }

}
