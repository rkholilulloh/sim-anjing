<?php
class Template_all {
	protected $_ci;
	function __construct(){
		$this ->_ci=&get_instance();
	}
	function display($view,$data=null){
		$data['_title']=$view['title'];
		$data['_head']=
		$this->_ci->load->view('template/header',$data,true);
		$data['_sidebar']=
		$this->_ci->load->view('template/sidebar',$data,true);
		$data['_content']=
		$this->_ci->load->view($view['content'],$data,true);
		$data['_foot']=
		$this->_ci->load->view('template/footer',$data,true);
		
		$this->_ci->load->view('/template/template.php',$data);
	}
}