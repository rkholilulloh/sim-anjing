            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h1 class="page-title"> Data Anjing </h1>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?= base_url() ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>anjing">Anjing</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li><a><?= $type ?></a></li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <?php
                        if(!empty($this->session->flashdata('crud_error'))){
                    ?>
                    <div class="alert alert-danger">
                        <?= $this->session->flashdata('crud_error') ?>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <h4><i class="fa fa-paw"></i> <?= $type ?> Anjing</h4>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <form action="<?= base_url() ?>anjing/<?php if($type=='Tambah') echo 'insert'; else echo 'update/'.$records->id ?>" method="POST" enctype='multipart/form-data'>
                                                <div class="form-group">
                                                    <label>Nama Anjing</label>
                                                    <input type="text" class="form-control" name="nama" value="<?= isset($records->nama) ? $records->nama : '' ?>">
                                                </div>
                                                <?php if($this->session->userdata('role')=='admin'){ ?>
                                                <div class="form-group">
                                                    <label>Pemilik</label>
                                                    <select id="select2-pemilik" class="form-control select2" name="id_pemilik">
                                                        <?php
                                                        if($type=='Edit'){
                                                        ?>
                                                        <option class="selected" value="<?= $records->id_pemilik ?>"><?= $records->nama_pemilik ?></option>
                                                        <?php } ?>
                                                        <option></option>
                                                    </select>
                                                </div>
                                                <?php } ?>
                                                <div class="form-group">
                                                    <label>Jenis Kelamin Anjing</label>
                                                    <select class="form-control select2 no-search" name="jenis_kelamin">
                                                        <option <?php if($type=='Edit' and $records->jenis_kelamin=='Laki-laki') echo 'selected' ?> value="Laki-laki">Laki-laki</option>
                                                        <option <?php if($type=='Edit' and $records->jenis_kelamin=='Perempuan') echo 'selected' ?> value="Perempuan">Perempuan</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Tanggal Lahir Anjing</label>
                                                    <input type="text" class="form-control date" name="tgl_lahir" value="<?= isset($records->tgl_lahir) ? $records->tgl_lahir : '' ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Ras Anjing</label>
                                                    <select id="select2-ras-anjing" class="form-control select2" name="id_ras">
                                                        <?php
                                                        if($type=='Edit'){
                                                        ?>
                                                        <option class="selected" value="<?= $records->id_ras ?>"><?= $records->ras ?></option>
                                                        <?php } ?>
                                                        <option></option>
                                                    </select>
                                                </div>
                                                <div class="from-group">
                                                    <label>Foto Anjing</label>
                                                    <div class="row">
                                                        <div class="col-sm-6">
                                                            <img class="foto <?= isset($records->foto) ? '' : 'hide' ?>" src="<?= isset($records->foto) ? base_url().$records->foto : '#' ?>">
                                                            <label class="btn btn-default btn-block btn-sm" for="upload-foto">Pilih foto</label>
                                                        </div>
                                                    </div>
                                                    <input id="upload-foto" type="file" class="form-control hide" name="foto">
                                                    <div class="space"></div>
                                                </div>
                                                <div class="form-group">
                                                    <label>Vaksin yang pernah diterima</label>
                                                    <?php
                                                    if($type=='Edit'){
                                                        foreach($faksin as $f){
                                                    ?>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <div class="form-group">
                                                                <label><small>Nama vaksin</small></label>
                                                                <input type="text" class="form-control input-nama-faksin" name="nama_faksin[]" value="<?= $f->nama ?>">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <div class="form-group">
                                                                <label><small>Jenis vaksin</small></label>
                                                                <select class="form-control select2 no-search input-jenis-faksin" name="jenis_faksin[]">
                                                                    <option class="selected" value="<?= $f->id_jenis_vaksin ?>"><?= $f->jenis_vaksin ?></option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <div class="form-group">
                                                                <label><small>Hapus</small></label>
                                                                <button type="button" class="btn btn-danger btn-block btn-faksin-delete"><i class="fa fa-times"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php }} else { ?>
                                                    <div class="row">
                                                        <div class="col-xs-6">
                                                            <div class="form-group">
                                                                <label><small>Nama vaksin</small></label>
                                                                <input type="text" class="form-control input-nama-faksin" name="nama_faksin[]">
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-4">
                                                            <div class="form-group">
                                                                <label><small>Jenis vaksin</small></label>
                                                                <select class="form-control select2 no-search input-jenis-faksin" name="jenis_faksin[]">
                                                                    <option></option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-xs-2">
                                                            <div class="form-group">
                                                                <label><small>Hapus</small></label>
                                                                <button type="button" class="btn btn-danger btn-block btn-faksin-delete"><i class="fa fa-times"></i></button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                                    <div class="clearfix"></div>
                                                    <button type="button" class="btn btn-sm btn-default btn-tambah-faksin"><i class="fa fa-plus"></i> Tambah vaksin lain</button>
                                                </div>
                                                <hr>
                                                <button class="btn btn-primary btn-block">Simpan</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->