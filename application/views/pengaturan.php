            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h1 class="page-title"> Pengaturan </h1>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?= base_url() ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>pengaturan">Pengaturan</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a>Password</a>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <?php
                        if(!empty($this->session->flashdata('crud_error'))){
                    ?>
                    <div class="alert alert-danger">
                        <?= $this->session->flashdata('crud_error') ?>
                    </div>
                    <?php } ?>
                    <?php
                        if(!empty($this->session->flashdata('crud_success'))){
                    ?>
                    <div class="alert alert-success">
                        <?= $this->session->flashdata('crud_success') ?>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <h4><i class="fa fa-cogs"></i> Pengaturan Password Pengguna</h4>
                                </div>
                                <div class="portlet-body">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="<?= base_url() ?>pengaturan/password">Password</a></li>
                                            <?php if($this->session->userdata('role')=='admin'){ ?>
                                            <li><a href="<?= base_url() ?>pengaturan/ras">Ras Anjing</a></li>
                                            <li><a href="<?= base_url() ?>pengaturan/vaksin">Jenis Vaksin</a></li>
                                            <?php } ?>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <form action="<?= base_url() ?>pengaturan/ganti_password" method="POST">
                                                            <div class="form-group">
                                                                <label>Password lama</label>
                                                                <input class="form-control" type="password" name="old_password">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Password baru</label>
                                                                <input class="form-control" type="password" name="password">
                                                            </div>
                                                            <div class="form-group">
                                                                <label>Ulangi password</label>
                                                                <input class="form-control" type="password" name="confirm_password">
                                                            </div>
                                                            <button class="btn btn-primary btn-block">Submit</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->