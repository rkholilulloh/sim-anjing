            <?php
                $last = $this->uri->segment($this->uri->total_segments());
                $class = $this->router->fetch_class();
            ?>
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item start <?php if($class == 'dashboard') echo 'active'; ?>">
                            <a href="<?= base_url() ?>" class="nav-link nav-toggle">
                                <i class="fa fa-home fa-2x"></i>
                                <span class="title">Dashboard</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            </a>
                        </li>
                        <li class="nav-item start <?php if($class == 'anjing') echo 'active'; ?>">
                            <a href="<?= base_url() ?>anjing" class="nav-link nav-toggle">
                                <i class="fa fa-paw fa-2x"></i>
                                <span class="title">Anjing</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item start ">
                                    <a href="<?= base_url() ?>anjing/tambah" class="nav-link ">
                                        <i class="fa fa-plus"></i>
                                        <span class="title">Tambah Data</span>
                                    </a>
                                </li>
                                <li class="nav-item start ">
                                    <a href="<?= base_url() ?>anjing" class="nav-link ">
                                        <i class="fa fa-circle-o"></i>
                                        <span class="title">Lihat Data</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php if($this->session->userdata('role')=='admin'){ ?>
                        <li class="nav-item start <?php if($class == 'pemilik') echo 'active'; ?>">
                            <a href="<?= base_url() ?>pemilik" class="nav-link nav-toggle">
                                <i class="fa fa-user fa-2x"></i>
                                <span class="title">Pemilik</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item start ">
                                    <a href="<?= base_url() ?>pemilik/tambah" class="nav-link ">
                                        <i class="fa fa-plus"></i>
                                        <span class="title">Tambah Data</span>
                                    </a>
                                </li>
                                <li class="nav-item start ">
                                    <a href="<?= base_url() ?>pemilik" class="nav-link ">
                                        <i class="fa fa-circle-o"></i>
                                        <span class="title">Lihat Data</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php } ?>
                        <li class="nav-item start <?php if($class == 'pengaturan') echo 'active'; ?>">
                            <a href="<?= base_url() ?>pengaturan" class="nav-link nav-toggle">
                                <i class="fa fa-cogs fa-2x"></i>
                                <span class="title">Pengaturan</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            </a>
                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
        