        <!-- START GLOBAL MODAL -->
        <div class="modal fade" id="modal-delete">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title"><b>Konfirmasi hapus data</b></h4>
                </div>
                <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data ini?</p>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button id="btn-go-delete" data-link="" type="button" class="btn btn-danger">HAPUS</button>
                </div>
            </div>
            </div>
        </div>
        <!-- END GLOBAL MODAL -->

        <!-- BEGIN FOOTER -->
        <div class="page-footer">
            <div class="page-footer-inner"> <?= date('Y') ?> &copy; All rights reserved
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
            <!-- END FOOTER -->
            <!--[if lt IE 9]>
<script src="<?= base_url() ?>assets/global/plugins/respond.min.js"></script>
<script src="<?= base_url() ?>assets/global/plugins/excanvas.min.js"></script> 
<script src="<?= base_url() ?>assets/global/plugins/ie8.fix.min.js"></script> 
<![endif]-->
            <!-- BEGIN CORE PLUGINS -->
            <script src="<?= base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
            <!-- END CORE PLUGINS -->
            <script src="<?= base_url() ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/global/plugins/select2/js/select2.full.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
            <script type="text/javascript" src="<?= base_url() ?>assets/leaflet/leaflet.js"></script>
	        <script type="text/javascript" src="<?= base_url() ?>assets/leaflet/choropleth.js"></script>
            <!-- BEGIN THEME GLOBAL SCRIPTS -->
            <script src="<?= base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
            <!-- END THEME GLOBAL SCRIPTS -->
            <!-- BEGIN THEME LAYOUT SCRIPTS -->
            <script src="<?= base_url() ?>assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/layouts/layout2/scripts/demo.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
            <!-- END THEME LAYOUT SCRIPTS -->
            <script>

                var markers = [];
                function refreshDashboardMap(map, data){
                    for(var i=0; i < markers.length; i++){
                        markers[i].remove();
                    }
                    markers = [];

                    for(var i=0; i < data.result.length; i++){
                        var coord = data.result[i].koordinat.split(",");
                        markers.push(new L.marker([coord[0],coord[1]]).bindPopup(`${data.result[i].alamat}<br><a data-index='${data.result[i].id}' class='btn-map-detail' href='javascript:;'>Lihat selengkapnya</a>`).addTo(map));
                    }
                }

                function readURL(input) {
                    if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('.foto').attr('src', e.target.result).removeClass('hide');
                    }

                    reader.readAsDataURL(input.files[0]);
                    }
                }

                $(document).ready(function(){

                    <?php
                    $last = $this->uri->total_segments();
                    $last_segment = $this->uri->segment($last);
                    ?>

                    $("#btn-go-delete").click(function(){
                        window.location = "<?= base_url().$last_segment ?>/delete/"+$(this).attr("data-link");
                    });

                    $(".btn-tambah").click(function(){
                        $("#modal-tambah .modal-title b").text($("#modal-tambah .modal-title b").text().replace("Edit","Tambah"));
                        $("#modal-tambah .modal-body input[type=text]").val("");
                        $("#modal-tambah .modal-body form").attr("action","<?= base_url().$last_segment ?>/insert/");
                        $("#modal-tambah").modal();
                    });

                    $(document).on('click', '.btn-delete', function(){
                        var id = $(this).parent().parent().attr("id").substring(4);

                        $("#btn-go-delete").attr("data-link", id);
                        $("#modal-delete").modal();
                    })

                    $(document).on('click', '.btn-edit', function(){
                        var id = $(this).parent().parent().attr("id").substring(4);

                        window.location = '<?= base_url().$this->router->fetch_class() ?>/edit/'+id;
                    });

                    $(document).on('click', '.btn-edit-modal', function(){
                        var id = $(this).parent().parent().attr("id").substring(4);
                        var name = $(this).parent().prev().text();

                        $("#modal-tambah .modal-title b").text($("#modal-tambah .modal-title b").text().replace("Tambah","Edit"));

                        $("#modal-tambah .modal-body form").attr("action","<?= base_url().$last_segment ?>/update/"+id);
                        $("#modal-tambah .modal-body input[type=text]").val(name);
                        $("#modal-tambah").modal();
                    });

                    $(document).on('click', '.btn-detail', function(){
                        var id = $(this).parent().parent().attr("id").substring(4);
                        $(".modal-loader").show();
                        $(".modal-body").hide();
                        $("#modal-detail").modal();

                        $.ajax({
                            url: '<?= base_url()."api/view_detail_".$this->router->fetch_class() ?>/'+id,
                            type: 'GET',
                            success: function(data){
                                var index = 0;
                                $.each(data.result, function(i, item){
                                    if(i!='unique_id' && i!='foto'){
                                        if(item==null)
                                            item = "";
                                        if(i=='koordinat'){
                                            var gmaps = "https://www.google.com/maps/search/?api=1&query="+item;
                                            item = `<a class='btn btn-sm btn-success' target='_blank' href=${gmaps}>Buka di Google Maps</a>`;
                                            $("#modal-detail .modal-body table tr:eq("+index+") td").html(item);
                                        } else {
                                            $("#modal-detail .modal-body table tr:eq("+index+") td").text(item);
                                        }
                                        index++;
                                    }
                                });
                                var public_uri = "<?= base_url() ?>view/"+data.result.unique_id;
                                var qr_url = `https://chart.googleapis.com/chart?cht=qr&chl=${public_uri}&chs=500x500&chld=M|2`;
                                console.log(data.result);
                                $("#foto-anjing").attr("src", data.result.foto);
                                $(".btn-get-qr").attr("href", qr_url);
                                $(".modal-body").show();
                                $(".modal-loader").hide();
                            },
                            error: function(err){
                                console.log(err);
                            }
                        });
                    });

                    $(document).on('click', '.btn-map-detail', function(){
                        var id = $(this).attr("data-index");
                        $(".modal-loader").show();
                        $(".modal-body").hide();
                        $("#modal-detail").modal();

                        $.ajax({
                            url: '<?= base_url()."api/view_detail_pemilik/" ?>'+id,
                            type: 'GET',
                            success: function(data){
                                var index = 0;
                                $.each(data.result, function(i, item){
                                    if(item==null)
                                        item = "";
                                    if(i=='koordinat'){
                                        var gmaps = "https://www.google.com/maps/search/?api=1&query="+item;
                                        item = `<a class='btn btn-sm btn-success' target='_blank' href=${gmaps}>Buka di Google Maps</a>`;
                                        $("#modal-detail .modal-body table tr:eq("+index+") td").html(item);
                                    } else {
                                        $("#modal-detail .modal-body table tr:eq("+index+") td").text(item);
                                    }
                                    index++;
                                });
                                $(".modal-loader").hide();
                                $(".modal-body").show();
                            },
                            error: function(err){
                                console.log(err);
                            }
                        });
                    });

                    $(".password-toggler").click(function(){
                        if($(this).hasClass("active")){
                            $(this).removeClass("active");
                            $(this).parent().prev().attr("type","password");
                        } else {
                            $(this).addClass("active");
                            $(this).parent().prev().attr("type","text");
                        }
                    });

                    $(".btn-change-pass").click(function(){
                        $(this).hide();
                        $(this).prev().removeClass("display-hide");
                    });

                    $("#upload-foto").on('change', function(){
                        readURL(this);
                    })

                    $("input.date").datepicker({
                        autoclose: true,
                        format: 'yyyy-mm-dd'
                    });

                    $(".select2").select2({
                        width: '100%'
                    });

                    $(".select2.no-search").select2({
                        minimumResultsForSearch: Infinity
                    });

                    function initSelect2(selectElementObj){
                        selectElementObj.select2({
                            minimumResultsForSearch: Infinity,
                            width: '100%',
                            ajax: {
                                url: '<?= base_url() ?>api/get_jenis_vaksin/',
                                type: "GET",
                                dataType: "json",
                                data: function(params){
                                    return {
                                        q: params.term
                                    }
                                },
                                processResults: function(data){
                                    return {
                                        results: $.map(data.result, function (item) {
                                            return {
                                                text: item.jenis,
                                                id: item.id
                                            }
                                        })
                                    }
                                }
                            }
                        });
                    }
                    initSelect2($(".input-jenis-faksin"));

                    $(document).on("click",".btn-faksin-delete", function(){
                        if($(".input-jenis-faksin").length > 1){
                            $(this).parent().parent().parent().remove();
                        } else {
                            $(".input-nama-faksin").val("");
                        }
                    })

                    $(".btn-tambah-faksin").click(function(){
                        html = `<div class="row">
                                    <div class="col-xs-6">
                                        <div class="form-group">
                                            <label><small>Nama vaksin</small></label>
                                            <input type="text" class="form-control input-nama-faksin" name="nama_faksin[]">
                                        </div>
                                    </div>
                                    <div class="col-xs-4">
                                        <div class="form-group">
                                            <label><small>Jenis vaksin</small></label>
                                            <select class="form-control select2 no-search input-jenis-faksin" name="jenis_faksin[]">
                                                <option>Swasta</option>
                                                <option>Pemerintah</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-2">
                                        <div class="form-group">
                                            <label><small>Hapus</small></label>
                                            <button type="button" class="btn btn-danger btn-block btn-faksin-delete"><i class="fa fa-times"></i></button>
                                        </div>
                                    </div>
                                </div>`;
                        $(html).insertBefore($(this));
                        initSelect2($(this).prev().find(".select2"));
                    })

                    $("#select2-pemilik").select2({
                        placeholder: 'Pilih pemilik anjing',
                        width: '100%',
                        ajax: {
                            url: "<?= base_url() ?>api/get_id_pemilik",
                            type: "GET",
                            dataType: "json",
                            data: function(params){
                                return {
                                    q: params.term
                                }
                            },
                            processResults: function(data){
                                return {
                                    results: $.map(data.result, function (item) {
                                        return {
                                            text: item.nama,
                                            id: item.id
                                        }
                                    })
                                }
                            }
                        }
                    });

                    $("#select2-jenis-anjing").select2({
                        placeholder: 'Pilih jenis anjing',
                        width: '100%',
                        ajax: {
                            url: "<?= base_url() ?>api/get_jenis_anjing",
                            type: "GET",
                            dataType: "json",
                            data: function(params){
                                return {
                                    q: params.term
                                }
                            },
                            processResults: function(data){
                                return {
                                    results: $.map(data.result, function (item) {
                                        return {
                                            text: item.jenis,
                                            id: item.id
                                        }
                                    })
                                }
                            }
                        }
                    });

                    $("#select2-ras-anjing").select2({
                        placeholder: 'Pilih ras anjing',
                        width: '100%',
                        ajax: {
                            url: "<?= base_url() ?>api/get_ras_anjing",
                            type: "GET",
                            dataType: "json",
                            data: function(params){
                                return {
                                    q: params.term
                                }
                            },
                            processResults: function(data){
                                return {
                                    results: $.map(data.result, function (item) {
                                        return {
                                            text: item.ras,
                                            id: item.id
                                        }
                                    })
                                }
                            }
                        }
                    });

                    $("#select2-kelurahan").select2({
                        placeholder: 'Pilih kelurahan',
                        width: '100%',
                        ajax: {
                            url: "<?= base_url() ?>api/get_kelurahan",
                            type: "GET",
                            dataType: "json",
                            data: function(params){
                                return {
                                    q: params.term,
                                    reg: $("#select2-kecamatan").val()
                                }
                            },
                            processResults: function(data){
                                return {
                                    results: $.map(data.result, function (item) {
                                        return {
                                            text: item.name,
                                            id: item.id
                                        }
                                    })
                                }
                            }
                        }
                    });

                    $("#select2-kecamatan").select2({
                        placeholder: 'Pilih kecamatan',
                        width: '100%',
                        ajax: {
                            url: "<?= base_url() ?>api/get_kecamatan",
                            type: "GET",
                            dataType: "json",
                            data: function(params){
                                return {
                                    q: params.term
                                }
                            },
                            processResults: function(data){
                                return {
                                    results: $.map(data.result, function (item) {
                                        return {
                                            text: item.name,
                                            id: item.id
                                        }
                                    })
                                }
                            }
                        }
                    });

                    
                    $("#datatable-anjing").DataTable({
                        serverSide: true,
                        ajax: "<?= base_url() ?>api/select_anjing",
                        order: [[0, 'asc']],
                        columns: [
                            {data: 'id_anjing'},
                            {data: 'nama'},
                            {data: 'jenis_kelamin'},
                            {data: 'ras'},
                            {data: 'nama_pemilik'},
                            {
                                data: null,
                                orderable: false,
                                defaultContent: "<a class='btn btn-sm btn-default btn-detail'>Detail</a><a class='btn btn-sm btn-primary btn-edit'>Edit</a><a class='btn btn-sm btn-danger btn-delete'>Hapus</a>"
                            }
                        ],
                        language: {
                            url: '<?= base_url() ?>assets/global/plugins/datatables/plugins/indonesian.json'
                        }
                    });

                    $("#datatable-pemilik").DataTable({
                        serverSide: true,
                        ajax: "<?= base_url() ?>api/select_pemilik",
                        order: [[0, 'asc']],
                        columns: [
                            {data: 'nama'},
                            {data: 'jenis_kelamin'},
                            {data: 'alamat'},
                            {data: 'nama_kelurahan'},
                            {data: 'district'},
                            {
                                data: null,
                                orderable: false,
                                defaultContent: "<a class='btn btn-sm btn-default btn-detail'>Detail</a><a class='btn btn-sm btn-primary btn-edit'>Edit</a><a class='btn btn-sm btn-danger btn-delete'>Hapus</a>"
                            }
                        ],
                        language: {
                            url: '<?= base_url() ?>assets/global/plugins/datatables/plugins/indonesian.json'
                        }
                    });

                    $("#datatable-jenis-anjing").DataTable({
                        serverSide: true,
                        ajax: "<?= base_url() ?>api/select_jenis_anjing",
                        order: [[0, 'asc']],
                        columns: [
                            {data: 'jenis'},
                            {
                                data: null,
                                orderable: false,
                                defaultContent: "<a class='btn btn-sm btn-primary btn-edit-modal'>Edit</a><a class='btn btn-sm btn-danger btn-delete'>Hapus</a>"
                            }
                        ],
                        language: {
                            url: '<?= base_url() ?>assets/global/plugins/datatables/plugins/indonesian.json'
                        }
                    });

                    $("#datatable-ras-anjing").DataTable({
                        serverSide: true,
                        ajax: "<?= base_url() ?>api/select_ras_anjing",
                        order: [[0, 'asc']],
                        columns: [
                            {data: 'ras'},
                            {
                                data: null,
                                orderable: false,
                                defaultContent: "<a class='btn btn-sm btn-primary btn-edit-modal'>Edit</a><a class='btn btn-sm btn-danger btn-delete'>Hapus</a>"
                            }
                        ],
                        language: {
                            url: '<?= base_url() ?>assets/global/plugins/datatables/plugins/indonesian.json'
                        }
                    });

                    $("#datatable-jenis-vaksin").DataTable({
                        serverSide: true,
                        ajax: "<?= base_url() ?>api/select_jenis_vaksin",
                        order: [[0, 'asc']],
                        columns: [
                            {data: 'jenis'},
                            {
                                data: null,
                                orderable: false,
                                defaultContent: "<a class='btn btn-sm btn-primary btn-edit-modal'>Edit</a><a class='btn btn-sm btn-danger btn-delete'>Hapus</a>"
                            }
                        ],
                        language: {
                            url: '<?= base_url() ?>assets/global/plugins/datatables/plugins/indonesian.json'
                        }
                    });

                    <?php if(isset($map)){ ?>
                    
                    var map = L.map('map', {
                        center: [<?= isset($lat) ? $lat : "-7.583" ?>,<?= isset($lng) ? $lng : "110.833" ?>],
                        zoom: 12,
                        layers: [
                            new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                                'attribution': 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
                            })
                        ]
                    });

                    <?php if($type=='Edit'){ ?>
                    marker = new L.marker([<?= $lat ?>,<?= $lng ?>], {draggable: 'true'}).addTo(map);
                    <?php } else { ?>
                    marker = new L.marker([-7.583,110.833], {draggable: 'true'}).addTo(map);
                    <?php } ?>
                    marker.on('dragend', function(e){
                        $("input[name=lat]").val(marker.getLatLng().lat);
                        $("input[name=lng]").val(marker.getLatLng().lng);
                    });

                    <?php } ?>

                    <?php if($this->router->fetch_class()=='dashboard'){ ?>
                    var map = L.map('map', {
                        center: ["-7.583", "110.833"],
                        zoom: 12,
                        layers: [
                            new L.TileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                                'attribution': 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors'
                            })
                        ]
                    });
                    
                    <?php
                        foreach($pemilik as $p){
                            if(!isset($p->koordinat)){
                                continue;
                            }
                            $coord = explode(',', $p->koordinat);
                    ?>
                    markers.push(new L.marker([<?= $coord[0] ?>,<?= $coord[1] ?>]).bindPopup("<?= $p->alamat ?><br><a data-index='<?= $p->id ?>' class='btn-map-detail' href='javascript:;'>Lihat selengkapnya</a>").addTo(map));
                    <?php } ?>

                    $(".select-dashboard").on('change', function(){
                        if($(this)[0].id=='select2-kecamatan'){
                            $("#select2-kelurahan").val("all").trigger('change');
                        }
                        $("#map-loader").show();
                        district = $("#select2-kecamatan").val();
                        kelurahan = $("#select2-kelurahan").val();
                        $.ajax({
                            url: '<?= base_url() ?>api/get_pemilik_by_location/',
                            data: `kecamatan=${district}&kelurahan=${kelurahan}`,
                            success: function(data){
                                refreshDashboardMap(map, data);
                                $("#map-loader").hide();
                            }
                        });
                    })
                    <?php } ?>
                })
            </script>
    </body>

</html>