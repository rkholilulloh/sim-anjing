            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h1 class="page-title"> Dashboard Admin </h1>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?= base_url() ?>">Dashboard</a>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <div class="row">
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 blue" href="<?= base_url() ?>anjing">
                                <div class="visual">
                                    <i class="fa fa-paw"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="100"><?= $anjing_total ?></span>
                                    </div>
                                    <div class="desc">Total Anjing</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 green" href="<?= base_url() ?>pemilik">
                                <div class="visual">
                                    <i class="fa fa-user"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="10"><?= $pemilik_total ?></span>
                                    </div>
                                    <div class="desc">Total Pemilik</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 red" href="#">
                                <div class="visual">
                                    <i class="fa fa-paw"></i>
                                </div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="100">100</span>
                                    </div>
                                    <div class="desc">Total Anjing</div>
                                </div>
                            </a>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                            <a class="dashboard-stat dashboard-stat-v2 yellow" href="#">
                                <div class="visual"></div>
                                <div class="details">
                                    <div class="number">
                                        <span data-counter="counterup" data-value="100">100</span>
                                    </div>
                                    <div class="desc">Total Anjing</div>
                                </div>
                            </a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <span class="caption-subject font-dark bold">Penyebaran Lokasi Pemilik Anjing</span>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div id="map" style="height: 300px;"></div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Filter Kecamatan</label>
                                                <select class="form-control select2 select-dashboard" id="select2-kecamatan">
                                                    <option value="all">Semua Kecamatan</option>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <label>Filter Kelurahan</label>
                                                <select class="form-control select2 select-dashboard" id="select2-kelurahan">
                                                    <option value="all">Semua Kelurahan</option>
                                                </select>
                                            </div>
                                            <div id="map-loader" class="display-hide"><h5><b><i class="fa fa-refresh fa-spin"></i> Refreshing map..</b></h5></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

            <!-- START PAGE DETAIL MODAL -->
            <div class="modal fade" id="modal-detail">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><b>Detail Pemilik</b></h4>
                    </div>
                    <div class="modal-loader">
                        <h4><b><i class="fa fa-circle-o-notch fa-spin"></i> Memuat data...</b></h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-striped table-hover">
                            <tr>
                                <th>Nama Lengkap</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Kelurahan</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Kecamatan</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>NIK</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>No. HP</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Koordinat</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Username</th>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
            </div>
            <!-- END PAGE DETAIL MODAL -->

        </div>
        <!-- END CONTAINER -->