<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Sistem Informasi Manajemen Anjing</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?= base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?= base_url() ?>assets/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/layouts/layout2/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?= base_url() ?>assets/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" />

        <link href="<?= base_url() ?>assets/custom.css" rel="stylesheet" type="text/css">
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class="page-header-fixed  page-sidebar-closed-hide-logo page-container-bg-solid page-sidebar-closed">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top hide">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="<?= base_url() ?>">
                        <img src="<?= base_url() ?>assets/layouts/layout2/img/logo-default.png" alt="logo" class="logo-default" /> 
                    </a>
                    <div class="menu-toggler sidebar-toggler hide">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN PAGE ACTIONS -->
                <!-- DOC: Remove "hide" class to enable the page header actions -->
                <div class="page-actions hide">
                    <div class="btn-group">
                        <button type="button" class="btn btn-circle btn-outline red dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-plus"></i>&nbsp;
                            <span class="hidden-sm hidden-xs">New&nbsp;</span>&nbsp;
                            <i class="fa fa-angle-down"></i>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="<?= base_url() ?>anjing/tambah">
                                    <i class="fa fa-paw"></i> Anjing Baru </a>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>pemilik/tambah">
                                    <i class="fa fa-user"></i> Pemilik baru </a>
                            </li>
                        </ul>
                    </div>
                    <!--<div class="label label-warning">
                        <b>Loading...</b>
                    </div>-->
                </div>
                <!-- END PAGE ACTIONS -->
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                    
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container" style="margin-top: 0px;">
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content" style="margin-left: 0px !important">
                    <!-- BEGIN PAGE HEADER-->
                    <h1 class="page-title text-center"> Data Anjing </h1>
                    
                    <div class="row">
                        <div class="col-md-3"></div>
                        <div class="col-md-6">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <h3><?= $this->security->xss_clean($anjing->nama) ?></h3>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-hover">
                                        <tr>
                                            <th>Nama Anjing</th>
                                            <td><?= $this->security->xss_clean($anjing->nama) ?></td>
                                        </tr>
                                        <tr>
                                            <th>Ras Anjing</th>
                                            <td><?= $this->security->xss_clean($anjing->ras) ?></td>
                                        </tr>
                                        <tr>
                                            <th>Pemilik Anjing</th>
                                            <td><?= $this->security->xss_clean($anjing->nama_pemilik) ?></td>
                                        </tr>
                                        <tr>
                                            <th>No. HP Pemilik</th>
                                            <td><?= $this->security->xss_clean($anjing->no_hp) ?></td>
                                        </tr>
                                        <tr>
                                            <th>Alamat</th>
                                            <td><?= $this->security->xss_clean($anjing->alamat).", ".$anjing->kecamatan.", ".$anjing->kabupaten ?></td>
                                        </tr>
                                    </table>
                                    <a target="_blank" href="https://www.google.com/maps/search/?api=1&query=<?= $anjing->koordinat ?>" class="btn btn-primary btn-block btn-map">Buka Alamat di Maps</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3"></div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

        </div>
        <!-- END CONTAINER -->
            <!--[if lt IE 9]>
            <script src="<?= base_url() ?>assets/global/plugins/respond.min.js"></script>
            <script src="<?= base_url() ?>assets/global/plugins/excanvas.min.js"></script> 
            <script src="<?= base_url() ?>assets/global/plugins/ie8.fix.min.js"></script> 
            <![endif]-->
            <!-- BEGIN CORE PLUGINS -->
            <script src="<?= base_url() ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
            <!-- END CORE PLUGINS -->
            <!-- BEGIN THEME GLOBAL SCRIPTS -->
            <script src="<?= base_url() ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
            <!-- END THEME GLOBAL SCRIPTS -->
            <!-- BEGIN THEME LAYOUT SCRIPTS -->
            <script src="<?= base_url() ?>assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/layouts/layout2/scripts/demo.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
            <script src="<?= base_url() ?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
            <!-- END THEME LAYOUT SCRIPTS -->
            <script>
                function isIOS() {
                    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
                    if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                        return true;
                    }

                    return false;
                }

                $(document).ready(function(){
                    if(isIOS()){
                        var current = $(".btn-map").attr("href");
                        $(".btn-map").attr("href",current.replace("https://","comgooglemapsurl://"));
                    }
                })
            </script>
    </body>

</html>