            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h1 class="page-title"> Data Anjing </h1>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?= base_url() ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a>Anjing</a>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- START ALERTS -->
                    <?php
                        if(!empty($this->session->flashdata('crud_success'))){
                    ?>
                    <div class="alert alert-success">
                        <?= $this->session->flashdata('crud_success') ?>
                    </div>
                    <?php } ?>
                    <?php
                        if(!empty($this->session->flashdata('crud_error'))){
                    ?>
                    <div class="alert alert-danger">
                        <?= $this->session->flashdata('crud_error') ?>
                    </div>
                    <?php } ?>
                    <!-- END ALERTS-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <h4><i class="fa fa-paw"></i> Tabel Anjing</h4>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <a href="<?= base_url() ?>anjing/tambah" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah baru</a>
                                        </div>
                                    </div></div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover" id="datatable-anjing">
                                            <thead>
                                            <tr>
                                                <th>ID Anjing</th>
                                                <th>Nama</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Ras Anjing</th>
                                                <th>Nama Pemilik</th>
                                                <th>Aksi</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

            <!-- START PAGE DETAIL MODAL -->
            <div class="modal fade" id="modal-detail">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><b>Detail Anjing</b></h4>
                    </div>
                    <div class="modal-loader">
                        <h4><b><i class="fa fa-circle-o-notch fa-spin"></i> Memuat data...</b></h4>
                    </div>
                    <div class="modal-body">
                        <div class="text-center">
                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    <img id="foto-anjing" src="#" class="foto">
                                </div>
                                <div class="col-md-2"></div>
                            </div>
                        </div>
                        <table class="table table-striped table-hover"> 
                            <tr>
                                <th>Pemilik Anjing</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>ID Anjing</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Nama Anjing</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin Anjing</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Tanggal Lahir Anjing</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Umur Anjing</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Ras Anjing</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Vaksin yang pernah diterima</th>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                        <a href="#" target="_blank" class="btn btn-primary btn-get-qr"><i class="fa fa-qrcode"></i> Simpan QR Code</a>
                    </div>
                </div>
                </div>
            </div>
            <!-- END PAGE DETAIL MODAL -->

        </div>
        <!-- END CONTAINER -->