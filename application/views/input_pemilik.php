            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h1 class="page-title"> Data Pemilik </h1>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?= base_url() ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>pemilik">Pemilik</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li><a><?= $type ?></a></li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <?php
                        if(!empty($this->session->flashdata('crud_error'))){
                    ?>
                    <div class="alert alert-danger">
                        <?= $this->session->flashdata('crud_error') ?>
                    </div>
                    <?php } ?>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <h4><i class="fa fa-user"></i> <?= $type ?> Pemilik</h4>
                                </div>
                                <div class="portlet-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <form action="<?= base_url() ?>pemilik/<?php if($type=='Tambah') echo 'insert'; else echo 'update/'.$records->id ?>" method="POST">
                                                <div class="form-group">
                                                    <label>Nama Lengkap</label>
                                                    <input type="text" class="form-control" name="nama" value="<?= isset($records->nama) ? $records->nama : '' ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Jenis Kelamin</label>
                                                    <select class="form-control select2 no-search" name="jenis_kelamin">
                                                        <option <?php if($type=='Edit' and $records->jenis_kelamin=='Laki-laki') echo 'selected' ?> value="Laki-laki">Laki-laki</option>
                                                        <option <?php if($type=='Edit' and $records->jenis_kelamin=='Perempuan') echo 'selected' ?> value="Perempuan">Perempuan</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat Asal</label>
                                                    <input type="text" class="form-control" name="alamat" value="<?= isset($records->asal) ? $records->asal : '' ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Alamat Domisili</label>
                                                    <input type="text" class="form-control" name="alamat" value="<?= isset($records->alamat) ? $records->alamat : '' ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Kecamatan</label>
                                                    <select id="select2-kecamatan" class="form-control select2" name="kecamatan">
                                                        <?php
                                                        if($type=='Edit'){
                                                        ?>
                                                        <option class="selected" value="<?= $records->kecamatan ?>"><?= $records->district ?></option>
                                                        <?php } ?>
                                                        <option></option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>Kelurahan</label>
                                                    <select id="select2-kelurahan" class="form-control select2" name="kelurahan">
                                                        <?php
                                                        if($type=='Edit'){
                                                        ?>
                                                        <option class="selected" value="<?= $records->kelurahan ?>"><?= $records->nama_kelurahan ?></option>
                                                        <?php } ?>
                                                        <option></option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label>NIK</label>
                                                    <input type="text" class="form-control" name="nik" value="<?= isset($records->nik) ? $records->nik : '' ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>No. HP</label>
                                                    <input type="text" class="form-control" name="no_hp" value="<?= isset($records->no_hp) ? $records->no_hp : '' ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Koordinat</label>
                                                    <div id="map" style="height: 300px;"></div>
                                                    <input type="hidden" name="lat" value="<?= isset($lat) ? $lat : '-7.583' ?>">
                                                    <input type="hidden" name="lng" value="<?= isset($lng) ? $lng : '110.833' ?>">
                                                </div>
                                                <hr>
                                                <div class="form-group">
                                                    <label>Username</label>
                                                    <input type="text" class="form-control" name="username" value="<?= isset($records->username) ? $records->username : '' ?>">
                                                </div>
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <div class="input-group <?php if($type=='Edit') echo 'display-hide' ?>">
                                                        <input type="password" class="form-control" name="password">
                                                        <span class="input-group-btn">
                                                            <button type="button" class="password-toggler btn btn-default"><i class="fa fa-eye"></i> Show</button>
                                                        </span>
                                                    </div>
                                                    <?php if($type=='Edit'){ ?>
                                                    <button type="button" class="btn btn-sm btn-default btn-change-pass">Reset password</button>
                                                    <?php } ?>
                                                </div>
                                                <hr>
                                                <button class="btn btn-primary btn-block">Simpan</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->