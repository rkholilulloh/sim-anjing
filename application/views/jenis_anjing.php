            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h1 class="page-title"> Pengaturan </h1>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?= base_url() ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a href="<?= base_url() ?>pengaturan">Pengaturan</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a>Jenis Anjing</a>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- START ALERTS -->
                    <?php
                        if(!empty($this->session->flashdata('crud_success'))){
                    ?>
                    <div class="alert alert-success">
                        <?= $this->session->flashdata('crud_success') ?>
                    </div>
                    <?php } ?>
                    <?php
                        if(!empty($this->session->flashdata('crud_error'))){
                    ?>
                    <div class="alert alert-danger">
                        <?= $this->session->flashdata('crud_error') ?>
                    </div>
                    <?php } ?>
                    <!-- END ALERTS-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <h4><i class="fa fa-cogs"></i> Data Jenis Anjing</h4>
                                </div>
                                <div class="portlet-body">
                                    <div class="tabbable-line">
                                        <ul class="nav nav-tabs">
                                            <li><a href="<?= base_url() ?>pengaturan/password">Password</a></li>
                                            <?php if($this->session->userdata('role')=='admin'){ ?>
                                            <li class="active"><a href="<?= base_url() ?>pengaturan/jenis">Jenis Anjing</a></li>
                                            <li><a href="<?= base_url() ?>pengaturan/ras">Ras Anjing</a></li>
                                            <li><a href="<?= base_url() ?>pengaturan/vaksin">Jenis Vaksin</a></li>
                                            <?php } ?>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active">
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <button class="btn btn-primary btn-tambah"><i class="fa fa-plus"></i> Tambah baru</button>
                                                    </div>
                                                </div></div>
                                                <div class="space"></div>
                                                <div class="table-responsive">
                                                    <table class="table table-bordered table-striped table-hover" id="datatable-jenis-anjing">
                                                        <thead>
                                                        <tr>
                                                            <th>Nama</th>
                                                            <th width="25%">Aksi</th>
                                                        </tr>
                                                        </thead>
                                                    </table>
                                                </div>    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

            <!-- START PAGE DETAIL MODAL -->
            <div class="modal fade" id="modal-tambah">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><b>Tambah Jenis Anjing</b></h4>
                    </div>
                    <div class="modal-body">
                        <form action="<?= base_url() ?>jenis/insert" method="POST">
                            <div class="row">
                                <div class="col-xs-8">
                                    <div class="form-group">
                                        <input type="text" placeholder="Jenis Anjing" class="form-control" name="jenis_anjing">
                                    </div>
                                </div>
                                <div class="col-xs-4">
                                    <button class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
            </div>
            <!-- END PAGE DETAIL MODAL -->

        </div>
        <!-- END CONTAINER -->