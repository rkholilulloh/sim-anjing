            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <h1 class="page-title"> Data Pemilik </h1>
                    <div class="page-bar">
                        <ul class="page-breadcrumb">
                            <li>
                                <i class="icon-home"></i>
                                <a href="<?= base_url() ?>">Dashboard</a>
                                <i class="fa fa-angle-right"></i>
                            </li>
                            <li>
                                <a>Pemilik</a>
                            </li>
                        </ul>
                    </div>
                    <!-- END PAGE HEADER-->
                    <!-- START ALERTS -->
                    <?php
                        if(!empty($this->session->flashdata('crud_success'))){
                    ?>
                    <div class="alert alert-success">
                        <?= $this->session->flashdata('crud_success') ?>
                    </div>
                    <?php } ?>
                    <?php
                        if(!empty($this->session->flashdata('crud_error'))){
                    ?>
                    <div class="alert alert-danger">
                        <?= $this->session->flashdata('crud_error') ?>
                    </div>
                    <?php } ?>
                    <!-- END ALERTS-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light">
                                <div class="portlet-title">
                                    <h4><i class="fa fa-user"></i> Tabel Pemilik</h4>
                                </div>
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                    <div class="row">
                                        <div class="col-md-4">
                                            <a href="<?= base_url() ?>pemilik/tambah" class="btn btn-primary"><i class="fa fa-plus"></i> Tambah baru</a>
                                        </div>
                                    </div></div>
                                    <div class="table-responsive">
                                        <table class="table table-bordered table-striped table-hover" id="datatable-pemilik">
                                            <thead>
                                            <tr>
                                                <th>Nama</th>
                                                <th>Jenis Kelamin</th>
                                                <th>Alamat</th>
                                                <th>Kelurahan</th>
                                                <th>Kecamatan</th>
                                                <th>Aksi</th>
                                            </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

            <!-- START PAGE DETAIL MODAL -->
            <div class="modal fade" id="modal-detail">
                <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><b>Detail Pemilik</b></h4>
                    </div>
                    <div class="modal-loader">
                        <h4><b><i class="fa fa-circle-o-notch fa-spin"></i> Memuat data...</b></h4>
                    </div>
                    <div class="modal-body">
                        <table class="table table-striped table-hover">
                            <tr>
                                <th>Nama Lengkap</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Jenis Kelamin</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Alamat</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Kelurahan</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Kecamatan</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>NIK</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>No. HP</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Koordinat</th>
                                <td></td>
                            </tr>
                            <tr>
                                <th>Username</th>
                                <td></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    </div>
                </div>
                </div>
            </div>
            <!-- END PAGE DETAIL MODAL -->

        </div>
        <!-- END CONTAINER -->