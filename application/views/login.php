<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon -->
     <link rel="icon" href="<?= base_url() ?>assets/img/favicon.png" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
    <!-- Style CSS -->
    <link rel="stylesheet" type="text/css" href="<?= base_url() ?>assets/style.css">
    <title>Sistem Informasi Anjing Kota Surakarta</title>
</head>

<body class="scroll-hidden">
    <div class="row">
        <div class="col-sm-12 col-md-5 form-right text-light form-left">
            <img src="<?= base_url() ?>assets/img/logo.png" class="img-fluid pb-5">
            <form action="<?= base_url() ?>login/auth" method="POST">
                <div class="form-group form-group-sm">
                    <label for="exampleInputPassword1">Username</label>
                    <input name="username" type="text" class="form-control" id="exampleInputPassword1" placeholder="Masukan Username">
                </div>
                <div class="form-group form-group-sm">
                    <label for="exampleInputPassword1">Password</label>
                    <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="Masukan Password">
                </div>
                <button type="submit" class="btn btn-warning shadow">Masuk</button>
            </form>
            <p class="pt-5">2019 © Dinas Pertanian, Ketahanan Pangan dan Perikanan Kota Surakarta</p>
        </div>
        <div class="col-sm-12 col-md-7 d-none d-sm-block">
            <div class="p-5">
                <img class="img-anjing img-fluid" src="<?= base_url() ?>assets/img/img-right.png">
            </div>
            </div>
        </div>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
</body>

</html>